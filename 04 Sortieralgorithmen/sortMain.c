/*
 * (c) Wegner, Weidner
 *
 *
 *	#1: n - Size of the input
 *  #2: c - Number of Probs each PE should get
 *  #3: its - Number of iterations
 *
 */

#include "sortAlgorithm.h"
#include "random.h"
 
//#define SEQ_SORT
 
 int main(int argc, char** argv)
 {
	unsigned int n = 0, c = 0;
    int peId, numProcs;
	int *unsortedInput;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &peId);
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
	
	// Size of the input
	n = atoi(argv[1]);
	// For the number of probs
	c = atoi(argv[2]);
     
     int its = atoi(argv[3]);
	
	assert(unsortedInput = malloc(n*sizeof(int)));
	
	//Initiallization of the input field
    initParallelRandomLEcuyer(424243, peId, numProcs);
    //randInitBound(unsortedInput, n, n*numProcs*10);
     randInit(unsortedInput, n);
     
#if SEQ_SORT
	//Sequentiel sort algorithm
	intSort(unsortedInput, n);
#else	
	//parallel sort
	sampleSort(n, unsortedInput, c, its);
#endif

	free(unsortedInput);
	MPI_Finalize();
 }