/*
 * (c) Wegner, Weidner
 */
#ifndef SORTIERALGORITHMEN_H 
#define SORTIERALGORITHMEN_H

#include <unistd.h>
#include <mpi.h> 
#include <math.h>
#include "timing.h"
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include "sort.h"

 void sampleSort(unsigned int n, int *unsortedInput, unsigned int c, int its);

#endif
