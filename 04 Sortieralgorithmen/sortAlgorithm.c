/*
 * (c) Wegner, Weidner
 *
 */
 
#include "sortAlgorithm.h"
#include <math.h>
#include "random.h"
#include <limits.h>
#include "timing.h"
 
#define ROOT 0
#define INTSORT
#define MAXINT 32
#define MAXCHAR 16
//#define DEBUGGING

#ifdef DEBUGGING
#define debug printf
#define d_sleep sleep
#else
#define debug(format, args...) ((void)0)
#define d_sleep(seconds) ((void)0)
#endif

void printArray(int *arr, int n){
#ifdef DEBUGGING
    for (int i=0; i<n; i++) {
        debug("%i\t", arr[i]);
    }
    debug("\n"); fflush(stdout);
#endif
}

int array_sum(int *array, int n){
    int res = 0;
    for (int i=0; i<n; i++) {
        res += array[i];
    }
    return res;
}

void exclusive_prefix(int *target, int *array, int numProcs){
    int sum = 0;
    for (int j=0; j<numProcs; j++) 
    {
        target[j] = sum; //start[j]-unsortedInput;
        sum += array[j];
    }
}

//int* my_Alltoallv(){
//    MPI_Alltoall( bucketlaenge, 1, MPI_INT, empfangslaenge, 1, MPI_INT, MPI_COMM_WORLD );   // (verifiziert: laeuft)
//    
//    d_sleep(peId/10);
//    debug("%i: Receive counts: ", peId);
//    printArray(empfangslaenge, numProcs);
//    int send_displ[numProcs], 
//    recv_displ[numProcs];
//    
//    exclusive_prefix(send_displ, bucketlaenge, numProcs);
//    exclusive_prefix(recv_displ, empfangslaenge, numProcs);
//    i_get = array_sum(empfangslaenge, numProcs);
//    
//    d_sleep(peId/10);
//    debug("%i: Send displacement: ", peId);
//    printArray(send_displ, numProcs);
//    d_sleep(peId/10);
//    debug("%i: Receive displacement: ", peId);
//    printArray(recv_displ, numProcs);
//    
//    
//    finalSequence = malloc(sizeof(int) * i_get);
//    MPI_Alltoallv(unsortedInput, bucketlaenge, send_displ, MPI_INT, 
//                  finalSequence, empfangslaenge, recv_displ, MPI_INT, MPI_COMM_WORLD)
//}
/*
 *
 */
void sampleSort(unsigned int n, int *unsortedInput, unsigned int c, int its)
{
	unsigned int i = 0, m = 0, k = 0;       //m - Number of samples; k - Number of pivot
	int peId, numProcs, globalSampleSize, intsize, charsize, bufsize,
    *allRandSamples;//, **start; 
    double startTime;
    int *empfangslaenge, 
        *bucketlaenge
//        *send_displ, 
//        *recv_displ
    ;
    double *times;

/*/ ---- Initialization + malloc part ---- /*/
	MPI_Comm_rank(MPI_COMM_WORLD, &peId);
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    assert(times = malloc(sizeof(double) * its));
    
    if(numProcs == 1){
        for (int run=0; run<its; run++) {
            startTime = MPI_Wtime();
            intSort(unsortedInput, n);
            times[run] = MPI_Wtime() - startTime;
            
            randInit(unsortedInput, n);
        }
        
        double pa = pruned_average(times, its, 0.25);
        printf("%i\t %f\t %i\t %i #PEs\t Time[us]\t n\t  Iterations\n",numProcs, 1e6*pa, n, its); 
		fflush(stdout);
        free(times);
        return;
    }
    
    
    
    int max_more = n * 1.1 - n;     // auf keinem PE sollten am ende mehr als n + max_more elemente landen!
	
	// Number of random samples for each PE
	m = (int) c *log(numProcs);
    int *randSamples;
    assert(randSamples = malloc(sizeof(int) * m));
    
    debug("%i: my sample size: %i\n", peId, m); 
	
    assert(m <= n);
	k = numProcs - 1 + 2; //+ 2 because of infinity 
	int *pivot;
    int *start[k];
    assert( pivot = malloc(k * sizeof(int)));
    globalSampleSize = m*numProcs;
	pivot[0] = -INT_MAX;
    pivot[k-1] = INT_MAX;
	if(peId == ROOT)
	{
        assert(allRandSamples 	= malloc(globalSampleSize*sizeof(int)));
        printArray(unsortedInput, n);
    }
    
    assert( empfangslaenge = malloc(sizeof(int) * numProcs));
    assert( bucketlaenge = malloc(sizeof(int) * numProcs));

	debug("Processor: %i\n", peId);
	printArray(unsortedInput,n);
	
    int *finalSequence, i_get=0;
/*/ ---- Select m random samples, collect them, chose and share them again  ---- /*/
    for (int run=0; run<its; run++) {
        startTime = MPI_Wtime();
        randInitBound(randSamples, m, n);
        
        // select m random items of the unsorted input
        for (i = 0; i < m; i++)
        { // randomly choose samples
            randSamples[i] = unsortedInput[randSamples[i]];
            debug("%i: my sample element at %i: %i\n", peId, i, randSamples[i]); 
        }
        
        d_sleep((int)peId/10);
        debug("Sample @ %i: ", peId); 
        printArray(randSamples, m);
        debug("%i: Starting the gather operation now...\n", peId); 
        fflush(stdout);

        MPI_Gather(randSamples, m, MPI_INT, allRandSamples, m, MPI_INT, ROOT, MPI_COMM_WORLD);

    /*/ ---- Pivot selection ---- /*/
        if(peId == ROOT)
        { // Sort of all samples of all PEs within root PE sequentially
            intSort(allRandSamples, globalSampleSize);
            for(i = 1; i < k-1; i++)
            { // Select pivot within the sample sort list
                pivot[i] =  allRandSamples[i*m]; // +1
            }
            debug("Root splitters: ");
            printArray(pivot+1, k-2);   /*/ --> the real splitter / */
        }
        // Broadcast the pivot to all other PEs
        MPI_Bcast(pivot, k, MPI_INT, ROOT, MPI_COMM_WORLD); //-1

        d_sleep((int)peId/10);
        debug("%i: my splitters are: \t", peId);
        printArray(pivot+1, k-2);   /*/ --> the real splitter /*/

    /*/ ---- Build Buckets for each PE according to splitters ---- /*/
        partition(unsortedInput, n, pivot, start, k);
        
/*/ "exclusive prefix sum" /*/
        
        int n_elements=0;
        for (int j=0; j<numProcs; j++) 
		{
            //send_displ[j] = n_elements;//*sizeof(int);
            debug("%i: Bucket %i:\t",peId,j);
            printArray(start[j], start[j+1] - start[j]);
            n_elements += start[j+1] - start[j];
            bucketlaenge[j] = start[j+1] - start[j];
        }
        
        debug("%i: Send counts: ", peId);
        printArray(bucketlaenge, numProcs);
        
    /*/ Distribute the number of elements of each bucket /*/
        MPI_Alltoall( bucketlaenge, 1, MPI_INT, empfangslaenge, 1, MPI_INT, MPI_COMM_WORLD );   // (verifiziert: laeuft)
        
        d_sleep(peId/10);
        debug("%i: Receive counts: ", peId);
        printArray(empfangslaenge, numProcs);
		int send_displ[numProcs], 
            recv_displ[numProcs];
        
        exclusive_prefix(send_displ, bucketlaenge, numProcs);
        exclusive_prefix(recv_displ, empfangslaenge, numProcs);
        i_get = array_sum(empfangslaenge, numProcs);

        d_sleep(peId/10);
        debug("%i: Send displacement: ", peId);
        printArray(send_displ, numProcs);
        d_sleep(peId/10);
        debug("%i: Receive displacement: ", peId);
        printArray(recv_displ, numProcs);
        

        finalSequence = malloc(sizeof(int) * i_get);
        MPI_Alltoallv(unsortedInput, bucketlaenge, send_displ, MPI_INT, 
                      finalSequence, empfangslaenge, recv_displ, MPI_INT, MPI_COMM_WORLD);
        debug("%i: Final sequence to Sort: ", peId);
        printArray(finalSequence, i_get);
        
        intSort(finalSequence, i_get);
        times[run] = MPI_Wtime() - startTime;
            d_sleep(peId);
        debug("%i: Sorted Sequence: \n", peId);
        printArray(finalSequence, i_get);
        assert(isGloballySorted(finalSequence, i_get));
        
        
        free(finalSequence);
		randInit(unsortedInput, n);
        
        if( (int)(i_get - n) > max_more){
            printf("# %i: Got %i elements too much to fit 1.1*n received elements.\n", peId, i_get-n-max_more ); 
            fflush(stdout);
        }
        
    }
    d_sleep(1);

    
    MPI_Barrier(MPI_COMM_WORLD);
	//d_sleep(peId*2);
    
    
    if(!peId)
	{
		double pa = pruned_average(times, its, 0.25);
        printf("%i\t %f\t %i\t %i #PEs\t #Time[us]\t #n\t  #Iterations\n",numProcs, 1e6*pa, n, its); 
		fflush(stdout);
    }
    
    //MPI_Barrier(MPI_COMM_WORLD);
    
//	free(start);
    free(empfangslaenge);
    free(bucketlaenge);
//    free(send_displ);
//    free(recv_displ);
    free(randSamples);
    free(times);
    free(pivot);
    if(!peId){
        free(allRandSamples);
    }
    //return;
    
// von Martin: ich hab mal den Block auskommentiert, da er beim kompilieren Fehler wirft oder bei Ausfuehrung abraucht
//	MPI_Pack_size(MAXINT, MPI_INT, MPI_COMM_WORLD, &intsize);
//	MPI_Pack_size(MAXCHAR, MPI_CHAR, MPI_COMM_WORLD, &charsize);
//	bufsize = intsize + charsize + (2*MPI_BSEND_OVERHEAD);
//	assert(buf = malloc(bufsize*sizeof(void)));
//
//	MPI_Buffer_attach(buf, bufsize);

//
//	// Kommunication  TODO
//	for(i = 0; i<numProcs; i++)
//	{	
//		sSize = start[i]- start[i-1]        // TODO: Beim ersten Schleifendurchlauf geschieht ein Zugriff auf start[-1], steht da was sinvolles drin?
//		MPI_Bsend(start[i], sSize , MPI_INT, i, i, MPI_COMM_WORLD);      
//	}
//	for(i = 0; i<numProcs; i++)
//	{
//		MPI_Recv(?, ?, MPI_INT, MPI_ANY_SOURCE, i, MPI_COMM_WORLD, &stat);
//	}
	/*/ ---- Verteilung der Buckets ---- /*/
    
	/* Idee:
	 Verteilung der Bucketgroessen mittels alltoall
	 alloziieren der Zielstruktur
	 alltoallv 
	 */

//	assert(MPI_Buffer_detach(&buf, &bufsize)); 
}
 
