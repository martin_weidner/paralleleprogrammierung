#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <unistd.h>
#include <mpi.h> 
#include <math.h>
#include "timing.h"
#include <matrix.h>
#include <stdbool.h>
#include <mkl_cblas.h>

#define ROOT 0
void matInitIdent(double *mat, int n) 
{
    for (int i = 0; i < n; ++i){
        for (int j=0; j<n; j++) {
            mat[n*i+j] = (double)(i+1)/(n*(j+1));
        }
    }
}
void vecInitIdent(double *vec, int n){
    for (int i=0; i<n; i++) {
        vec[i] = (double)i+1;
    }
}
void verifyIdentVector(double *vec, int n){
    printf("# Verifying vector:\t");
    double variance = 0.001;
    for (int i=0; i<n; i++) {
        if(fabs(vec[i]-(double)(i+1))-variance > 0){
            printf("------------ Calculated vector incorrect! ------------ \n");
            fflush(stdout);
            return;
        }
    }
    printf("------------ Vector correct. ------------\n");fflush(stdout);
}

int main(int argc, char** argv){
    int myRank, numProcs, n, k, linesPerPE;
    double *mat, *x, *y, *localMat, *localY;

    MPI_Init(&argc, &argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);

    n = atoi(argv[1]);
    linesPerPE = (int) (n/numProcs);
/*/ --- Allocate --- /*/
    assert(x = malloc(n*sizeof(double)));
    assert(localY = malloc(linesPerPE*sizeof(double)));
    assert(localMat = malloc(n*linesPerPE*sizeof(double)));
    if(ROOT == myRank){
        assert(mat = malloc(n*n*sizeof(double)));
        matInitIdent(mat, n);
        assert(y = malloc(n*sizeof(double)));
        vecInitIdent(x, n);
    }
/*/ --- Distribute --- /*/
    MPI_Scatter(mat, n*linesPerPE, MPI_DOUBLE, localMat, n*linesPerPE, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
    MPI_Bcast(x, n, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
/*/ --- Local calculation --- /*/
    cblas_dgemv(CblasRowMajor, CblasNoTrans, (linesPerPE), (n), 1.0, (localMat), (n), (x), 1, 0.0, (localY), 1) ;    
/*/ --- Collect /*/
    MPI_Gather(localY, linesPerPE, MPI_DOUBLE, y, linesPerPE, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
    if (myRank==ROOT) {
        verifyIdentVector(y, n);
    }

    free(localMat);
    free(x);
    free(localY);
    if(ROOT == myRank){
        free(mat);
        free(y);
    }
    MPI_Finalize();
}