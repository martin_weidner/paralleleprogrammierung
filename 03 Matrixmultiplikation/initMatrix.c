/* (c) 2011 Franziska Wegner, Martin Weidner */
/* 
 * simple matrix initialization (serial version)
 */
#include "initMatrix.h"

/* determine random double between 0 and n-1 */
#define randDouble(n) ((double)(nextRandomLEcuyer() * n))

/* 
 * null matrix
 * every entry is null
 */
void matInitNull(double *mat, unsigned int n)
{
	memset(mat, 0, (n*n)*sizeof(double));
}

/* 
 * identity matrix
 * on diagonal 1s
 */
void matInitIdentity(double *mat, unsigned int n)
{
	int dim=0;
	matInitNull(mat,n);
	for(dim=0; dim<n; dim++)
	{
		mat[dim*(n)+dim]=1;
	}
}

/* 
 * matrix with random entries
 */
void matInitRandom(double *mat, unsigned int n, int seed)
{
	int length=0;
	initRandomLEcuyer(seed);
	for(length=0; length<(n*n); length++)
	{
		mat[length] = randDouble(99);
	}
}

/*
 * matrix A with enty aicol=i/(col+1)
 */
void matInitMatrixA(double *mat, unsigned int n)
{
	int row=0, col=0;
	for(row=0; row<n; row++)
	{
		for(col=0; col<n; col++)
		{
			mat[n*row+col]= (double)row/(col+1);       
		}
	}
}

/*
 * matrix B with enty bicol=(i+1)/(n*(col+1))
 */
void matInitMatrixB(double *mat, unsigned int n)
{
	int row=0, col=0;
	for(row=0; row<n; row++)
	{
		for(col=0; col<n; col++)
		{
			mat[n*row+col]= (double)(row+1)/(n*(col+1));     
		}
	}
}

/*
 *
 */
void matInitEnumerate(double *mat, int n) 
{
    for (int i = 0; i < n*n; ++i)
	{
        mat[i]=i;
    }
}

void matInitIdent(double *mat, int n) 
{
    for (int i = 0; i < n; ++i){
        for (int j=0; j<n; j++) {
            mat[n*i+j] = (double)(i+1)/(n*(j+1));
        }
    }
}

void vecInitEnumerate(double *vec, int n){
    for (int i=0; i<n; i++) {
        vec[i] = i;
    }
}

void vecInitIdent(double *vec, int n){
    for (int i=0; i<n; i++) {
        vec[i] = (double)i+1;
    }
}

void vecInitZero(double *vec, int n){
    for (int i=0; i<n; i++) {
        vec[i] = 0;
    }
}

/*
 *
 */
void matInit12(double *mat, int n) 
{
    int j=1;
    for (int i = 0; i < n*n; ++i) 
	{
        mat[i]	= (j+1)*3;
        j 		= (j+1) % 2;
    }
}
