/* (c) 2011 Franziska Wegner, Martin Weidner */
/* 
 * matrix operations (serial and parallel version)
 */
#include "matrixOperation.h"
#include "timing.h"

void verifyIdentVector(double *vec, int n);
void verifyIdentMatrix(double *mat, int n);
 /*
 * slow matrix multiplication
 */
void matMatMultSeqFor(unsigned int n, double *matA, double *matB, double *matC)
{
	unsigned int col=0, row=0, k=0; 
	for(row = 0; row < n; row++)
	{
        for(col = 0; col < n; col++)
		{
			for(k = 0; k < n; k++)
			{
				matC[n*row+col] += matA[n*row+k]*matB[k*n+col];
			}
		}
	}
} 

/*
 *	Matrix-Matrix-Multiplication of the library <matrix.h>
 */
void matMatMultSeq(unsigned int n, double *matA, double *matB, double *matC)
{
	matMatMult(n, matA, matB, matC);
}

/*
 *	Vector-Matrix-Multiplication of the library <matrix.h>
 */
void matVecMultSeq(unsigned int n, double *matA, double *x, double *y)
{
	matVecMult(n, matA, x, y);
}

/*
 *
 */
void memAlloc(double **matA, double **matB, double **matC, int n)
{
    assert(*matA = malloc((n*n)*sizeof(double)));
    assert(*matB = malloc((n*n)*sizeof(double)));
    assert(*matC = malloc((n*n)*sizeof(double))); 
}

/*
 *
 */
bool validateMatrizes(double *correctC, double *localC, int n)
{
    double e = 0.001;
    unsigned int offset;
    bool flag = true;
    for (int i=0; i<n; i++) 
	{
        for (int j=0; j<n; j++) 
		{
            offset = i*n+j;
            if(fabs( correctC[offset] - localC[offset] ) -e > 0)
			{
                flag = false;
            }
        }
    }
    return flag;
}

/*
 * Display a matrix
 */
void displayMatrix(unsigned int n, double *mat)
{
	unsigned int col=0, row=0;
	for(row = 0; row < n; row++)
	{
		for(col = 0; col < n; col++)
		{
			printf("\t\t%.4f",mat[n*row+col]);
		}
		printf("\n");
		fflush(stdout);
    }
}


/*
 * Display a vectors
 */
void displayVectorT(unsigned int n, double *vector)
{
	unsigned int count=0;
	for(count = 0; count < n; count++)
	{
		printf("\t\t%.4f",vector[count]);
	}
	printf("\n");
	fflush(stdout);
}

/*
 *	Matrix-Matrix-Multiplication (n x n) parallel with MPI
 */
void matMatMultPar( int n, double* matA, double* matB, double *matC)
{
    double *localA, *localB, *localC;
	unsigned i = 0, j = 0, k = 0;
	double nowTime, startTime, slowAlgTime, paraAlgTime;
	int peId, tRank, numProcs, targetRank, reorder, offset;
	int sqrtP, blockDim, dim[DIM], myCoord[DIM], period[DIM], tCoord[DIM];
    int matrixSizes[ROUNDS] = {120, 480, 1920}; 
    int dest, source, *displs;
	MPI_Request *requests, *req; 		
    MPI_Status *stat, *statuses;
	MPI_Datatype MPI_line, MPI_subMatrix, MPI_subColumn;
    MPI_Comm comm;
	MPI_Status status;
    
    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &peId);
    
   	requests 	= malloc(2*numProcs*sizeof(MPI_Request)); ;
	req 		= malloc(2*sizeof(MPI_Request));
    stat 		= malloc(2*sizeof(MPI_Status));
    statuses 	= malloc(2*numProcs*sizeof(MPI_Status));
	
	sqrtP = (int) sqrt(numProcs);
    
    /*/ Calculate with 120x120, 480x480, 1920x1920 /*/
    blockDim 	= (int) n 	/ sqrtP;
    dim[0] 		= dim[1] 	= sqrtP;
    reorder 	= 0;
    period[0] 	= period[1] = 1;
	
    MPI_Cart_create( MPI_COMM_WORLD, DIM, dim, period, reorder, &comm);
	
    memAlloc(&localA, &localB, &localC, blockDim); 
    matInitNull(localC, blockDim);
    MPI_Type_vector(1, blockDim, 0, MPI_DOUBLE, &MPI_line);
    MPI_Type_commit(&MPI_line);
    MPI_Type_vector(blockDim, 1, (n/blockDim), MPI_line, &MPI_subMatrix);
    MPI_Type_commit(&MPI_subMatrix);
    
    displs = malloc(numProcs * sizeof(int));

    if(peId == 0)
	{ // Distribute sub-matrizes
        for(i = 0; i < sqrtP; i++)
		{
            for(j = 0; j < sqrtP; j++)
            {
                offset = (n*blockDim*i)+(j*blockDim);
                tCoord[0] = i;
                tCoord[1] = j;
                
                MPI_Cart_rank(comm, tCoord, &tRank);
                displs[tRank] = offset;
                
                MPI_Isend(&matA[offset], 1, MPI_subMatrix, tRank, COMM_A, comm, &requests[targetRank]);
                MPI_Isend(&matB[offset], 1, MPI_subMatrix, tRank, COMM_B, comm, &requests[numProcs+targetRank]);
            }
        }
    }
    MPI_Irecv(localA, blockDim*blockDim, MPI_DOUBLE, 0, COMM_A, comm, &req[0]);
    MPI_Irecv(localB, blockDim*blockDim, MPI_DOUBLE, 0, COMM_B, comm, &req[1]);
    MPI_Waitall(2, req, stat); 
    
#ifdef DISPLAY_MATRICES
    sleep(peId);
    printf("I am PE %d with local matrices \n\tA:\n", peId);
    displayMatrix(blockDim, localA);
    printf("\tB:\n");
    displayMatrix(blockDim, localB);
#endif
    
    /*/ Create initial distribution /*/
    MPI_Cart_coords( comm, peId, DIM, myCoord );
    
    startTime = MPI_Wtime();
    MPI_Cart_shift(comm, 1, -1*myCoord[0], &source, &dest);
    MPI_Sendrecv_replace(localA, blockDim*blockDim, MPI_DOUBLE, dest, 0, source, 0, comm, &status);
    MPI_Cart_shift(comm, 0, -1*myCoord[1], &source, &dest);
    MPI_Sendrecv_replace(localB, blockDim*blockDim, MPI_DOUBLE, dest, 1, source, 1, comm, &status);
   
    for ( k = 0; k < sqrtP; k++) 
	{ // Multiply + Rotate
        MPI_Barrier(comm);
        MPI_Cart_shift(comm, 1, -1, &source, &dest);
        MPI_Sendrecv_replace(localA, blockDim*blockDim, MPI_DOUBLE, dest, 0, source, 0, comm, &status);
        MPI_Barrier(comm);
        MPI_Cart_shift(comm, 0, -1, &source, &dest);
        MPI_Sendrecv_replace(localB, blockDim*blockDim, MPI_DOUBLE, dest, 1, source, 1, comm, &status);
        matMatMultAdd(blockDim, localA, localB, localC);
    }
	
    /*/ ---------------- Time Measurement: reduce-op for average time ------------------------ /*/ 
    nowTime = MPI_Wtime()-startTime;
    MPI_Reduce(&nowTime, &paraAlgTime, 1, MPI_DOUBLE, MPI_SUM, 0, MPI_COMM_WORLD);
	
    if(peId == 0)
	{
        paraAlgTime *= 1e6; 
        paraAlgTime /= numProcs;
        printf("\n#PEs\t #Time[us]\t #n\t #N\n");
        printf("%i\t %f\t %i\t %i\n\n",numProcs, paraAlgTime, n, blockDim); fflush(stdout);
    }
    /*/ ---------------- --------------------------------------------------------------------- /*/
    
    MPI_Request myRequest;
    MPI_Isend( localC, blockDim*blockDim, MPI_DOUBLE, 0, COMM_C, comm, &myRequest);
	
    if(peId == 0)
	{
        for ( i = 0; i < numProcs; i++) 
		{
            MPI_Irecv( &matC[displs[i]], 1, MPI_subMatrix, i, COMM_C, comm, &requests[i]);
        }
    }
	
    MPI_Wait(&myRequest, stat);
	
    if(peId == 0)
	{
        MPI_Waitall(numProcs, requests, statuses);
    }
    
#ifdef DISPLAY_MATRICES
    sleep(peId);
    printf("R-I am PE %d. C:\n", peId);   
	displayMatrix(blockDim, localC);
	printf("\n");
	MPI_Barrier(comm);
#endif

    if(peId == 0)
    {
#ifdef DISPLAY_MATRICES
        printf("Final Matrix C:\n");    
		displayMatrix(n, matC);
        matInitNull(matC, n);           
		matMatMultSeq(n, matA, matB, matC);
        printf("\nCorrect C to compare\n"); 
		displayMatrix(n, matC);
#endif
        verifyIdentMatrix(matC, n);
    }
 
	MPI_Type_free(&MPI_line);
	MPI_Type_free(&MPI_subMatrix);
    MPI_Comm_free(&comm);
    
    free(localA); 
	free(localB); 
	free(localC);
}

//void distributeSubMatrix(double *offset, int numProcs, int peId, MPI_Comm *comm, ){
//    
//}
/*
 *	Vector-Matrix-Multiplication parallel with MPI
 * 	matA is a n x n matrix
 *	x and y are two n x 1 column vectors
 */
void matVecMultPar(unsigned int n, double *matA, double *x, double *y, int k)
{
	double *localA, *localX, *localY, *tempLocalY, *yControl;
	double nowTime, startTime, slowAlgTime, paraAlgTime;
	int peId = 0, numProcs= 0, sqrtP = 0, blockDim = 0, color = 0, i = 0, j = 0;
	int tRank, targetRank, reorder, offset = 0;
	int dim[DIM], myCoord[DIM], period[DIM], tCoord[DIM];
    int dest, source, *displs;
	MPI_Request *requests, *req; 		
    MPI_Status *stat, *statuses;
	MPI_Datatype MPI_line, MPI_subMatrix, MPI_vectorline;
    MPI_Comm comm, commHorizontal, commVertical, commDiagonal;
	MPI_Status status;	
    
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &peId);
	
	sqrtP 		= (int) sqrt(numProcs);
    blockDim 	= (int) n 	/ sqrtP;
   	assert(requests 	= malloc(2*numProcs*sizeof(MPI_Request)));
	assert(req 			= malloc(2*sizeof(MPI_Request)));
    
    //if(peId==ROOT){
    //    assert(yControl = malloc(n*sizeof(double)));   
    //}
    assert(stat 		= malloc(2*sizeof(MPI_Status)));
    assert(statuses 	= malloc(2*numProcs*sizeof(MPI_Status)));
	
    dim[0] 		= dim[1] 	= sqrtP;
    reorder 	= 0;
    period[0] 	= period[1] = 1;

    MPI_Cart_create( MPI_COMM_WORLD, DIM, dim, period, reorder, &comm);
    MPI_Cart_coords(comm, peId, DIM, myCoord);
    
    bool isDiagPe = (myCoord[0]==myCoord[1]);
	assert(localA = malloc((blockDim*blockDim)*sizeof(double)));
	assert(localX = malloc(blockDim*sizeof(double)));
	assert(localY = malloc(blockDim*sizeof(double)));
	assert(displs = malloc(numProcs * sizeof(int)));
	
    /*/ dabei gibt es natuerlich nen segmentation fault! /*/
//	matInitNull(localY, blockDim);
    vecInitZero(localY, blockDim);
	
    MPI_Type_vector(1, blockDim, 0, MPI_DOUBLE, &MPI_line);
    MPI_Type_commit(&MPI_line);
    MPI_Type_vector(blockDim, 1, (n/blockDim), MPI_line, &MPI_subMatrix);
    MPI_Type_commit(&MPI_subMatrix);
    
/*/ Wollen wir vielleicht das MAtrix-Verteilen auslagern? /*/
		
    if(peId == 0)
	{ // Distribute sub-matrizes
#ifdef DISPLAY_MATRICES
        printf("%i: Initial vector:\n", peId); 
        displayVectorT( n, x);
        //matVecMultSeq(n, matA, x, yControl);
        //printf("%i Correct final vector:\n", peId);
        //displayVectorT(n, yControl);
#endif
        for(i = 0; i < sqrtP; i++)
		{
            for(j = 0; j < sqrtP; j++)
            {
                offset = (n*blockDim*i)+(j*blockDim);
                tCoord[0] = i;
                tCoord[1] = j;
                MPI_Cart_rank(comm, tCoord, &tRank);
                displs[tRank] = offset;
                MPI_Isend(&matA[offset], 1, MPI_subMatrix, tRank, COMM_A, comm, &requests[targetRank]);
            }
        }
    }
    MPI_Irecv(localA, blockDim*blockDim, MPI_DOUBLE, 0, COMM_A, comm, &req[0]);
    MPI_Waitall(1, req, stat); 
/*/ Verteilung der Matrizen fertig /*/
    
/*/ Sende Vektorteile an Diagonal-PEs /*/
	MPI_Comm_split(MPI_COMM_WORLD, (int)isDiagPe, peId, &commDiagonal);
    if(isDiagPe){
        MPI_Scatter( x, blockDim, MPI_DOUBLE, localX, blockDim, MPI_DOUBLE, ROOT, commDiagonal);
    }
	MPI_Comm_split(comm, myCoord[1], peId, &commVertical);
	MPI_Comm_split(comm, myCoord[0], peId, &commHorizontal);
    double *timeArray, lastTime, pa;
    assert( timeArray = malloc(k*sizeof(double)));

    
    for (int i=0; i<k; i++) {
//        for (int j=0; j<100; j++) {
        	lastTime = MPI_Wtime();
            MPI_Bcast(localX, blockDim, MPI_DOUBLE, myCoord[1], commVertical);
            matVecMultSeq(blockDim, localA, localX, localY);
            MPI_Reduce(localY, localX, blockDim, MPI_DOUBLE, MPI_SUM, myCoord[0], commHorizontal);
            timeArray[i] = MPI_Wtime() - lastTime;
//        }
    }
    
	if(isDiagPe)
	{
		MPI_Gather(localX, blockDim, MPI_DOUBLE, y, blockDim, MPI_DOUBLE, ROOT, commDiagonal);
	}
	if(peId == ROOT)
	{
#ifdef DISPLAY_MATRICES
		displayVectorT(n,y);
#endif
        verifyIdentVector(y, n);
        pa = pruned_average(timeArray, k, 0.25);
        printf("%g# time[us], k=%i\n",
               1e6 * pa, k);
	}
    free(requests); 
    free(req); 
    free(stat); 
    free(statuses);
    free(localA);
    free(localX);
    free(localY);
    MPI_Comm_free(&comm);
    MPI_Comm_free(&commHorizontal);
    MPI_Comm_free(&commVertical);
    MPI_Comm_free(&commDiagonal);
}
void verifyIdentMatrix(double *mat, int n){
    printf("# Verifying matrix:\t");
    double variance = 0.001;
    for (int i=0;i<n; i++) {
        for (int j=0; j<n; j++) {
            if(fabs(mat[i*n+j]-(double)i/(j+1))-variance > 0){
                printf("------------ Calculated matrix incorrect! ------------ \n");
                fflush(stdout);
                return; 
            }
        }
    }
    printf("------------ Matrix correct. ------------\n");fflush(stdout);
}
void verifyIdentVector(double *vec, int n){
    printf("# Verifying vector:\t");
    double variance = 0.001;
    for (int i=0; i<n; i++) {
        if(fabs(vec[i]-(double)(i+1))-variance > 0){
            printf("------------ Calculated vector incorrect! ------------ \n");
            fflush(stdout);
            return;
        }
    }
    printf("------------ Vector correct. ------------\n");fflush(stdout);
}