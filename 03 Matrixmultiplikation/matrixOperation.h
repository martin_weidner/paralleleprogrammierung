#ifndef MATRIXOPERATION_H
#define MATRIXOPERATION_H

#include <unistd.h>
#include <mpi.h> 
#include <math.h>
#include "timing.h"
#include <matrix.h>
#include <stdbool.h>
#include "initMatrix.h"

#define DIM     2
#define COMM_A 42
#define COMM_B 41
#define COMM_C 43
#define ROUNDS  3
#define ROOT	0

//#define DISPLAY_MATRICES

void matMatMultSeqFor(unsigned int n, double *matA, double *matB, double *matC);
void matMatMultSeq(unsigned int n, double *matA, double *matB, double *matC);
void matVecMultSeq(unsigned int n, double *matA, double *x, double *y);

void matMatMultPar( int n, double* matA, double* matB, double *matC);
void matVecMultPar(unsigned int n, double *matA, double *x, double *y, int k);

void displayMatrix(unsigned int n, double *mat);
void memAlloc(double **matA, double **matB, double **matC, int n);
bool validateMatrizes(double *correctC, double *localC, int n);

#endif /*MATRIXOPERATION_H*/