/* (c) 2011 Franziska Wegner, Martin Weidner */
/* 
 * simple matrix multiplication (serial version)
 * and time measurement against library
 *
 * #1: n - Dimension for the n x n matrices
 */

#include "matrixOperation.h"

//#define MATMULT
int main(int argc, char** argv)
{

	double *matA, *matB, *matC, *x, *y;
	int n = 0, peId, k;
	
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &peId);
	
	n = atoi(argv[1]);
	assert(n > 1);
	
	if(peId == 0)
    {	
#ifdef MATMULT
        memAlloc(&matA, &matB, &matC, n);
        matInitMatrixA(matA, n);
        matInitIdent(matB, n);
#endif
#ifdef DISPLAY_MATRICES
        printf("Matrix A: \n");
        displayMatrix(n, matA);
        printf("\n"); 
		fflush(stdout);
		
		printf("Matrix B: \n");
        displayMatrix(n, matB);
        printf("\n"); 
		fflush(stdout);
#endif
    }
#ifdef MATMULT	
	// parallel n x n matrix multiplication
	matMatMultPar(n, matA, matB, matC);
	
	if(peId == 0 )
	{
        free(matA); 
		free(matB); 
		free(matC);
    }
#else
    if(peId == 0){
        assert(matA = malloc(n*n*sizeof(double)));
        assert(x = malloc(n*sizeof(double)));
        assert(y = malloc(n*sizeof(double)));
        matInitIdent(matA, n);
#ifdef DISPLAY_MATRICES
        printf("Matrix A: \n");
        displayMatrix(n, matA);
        printf("\n"); 
		fflush(stdout);
#endif
        vecInitIdent(x, n);
    }
    k = atoi(argv[2]);
    assert(k>0);
	matVecMultPar(n, matA, x, y, k);
    if(peId == 0){
		free(matA); 
        free(x);
        free(y);
    }
#endif
    
	MPI_Finalize();	
	return 0;
}


