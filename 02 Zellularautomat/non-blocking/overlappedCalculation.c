/* (c) 1996,1997 Peter Sanders, Ingo Boesnach */
/* simulate a cellular automaton (serial version)
 * periodic boundaries
 *
 * use graphical display
 * 
 * #1: Number of lines 
 * #2: Number of iterations to be simulated
 * #3: Length of period (in iterations) between displayed states. 
 *     0 means that no state is displayed.
 * #4: Name of X Display. E.g. "i90xxx.ira.uka.de:0.0"
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h> 

#include "bitmap.h"
#include "random.h"
#include "timing.h"
#include <assert.h>

/* horizontal size of the configuration */
#define XSIZE 2048


/* "ADT" State and line of states (plus border) */
typedef char State;
typedef State Line[XSIZE + 2];
#define STATE MPI_CHAR
#define UPWARDS  42
#define DOWNWARDS  21
/* every how many simulation cycles shall the display be updated ? */
int displayPeriod;
int numProcs, myRank, localLines;

/* determine random integer between 0 and n-1 */
#define randInt(n) ((int)(nextRandomLEcuyer() * n))

/* --------------------- debugging and I/O ---------------------------- */

/* display configuration stored in buf which consists
 * of lines lines on a window using bitmap.h 
 */
static void displayConfig(Line *buf, int lines)
{  int i, x, y;
    if(myRank != 0) return;
   /* pack data (-> 8 pixels/byte; necessary for bitmapDisplay) */
   i = 0;
   for (y = 1;  y <= lines;  y++) {
      for (x = 1;  x <= XSIZE;  x+= 8, i++) {
         /*>^ Dirty trick: this reads up to 7 elements beyond
          *   a line boundary. This is OK because there is always
          *   legally accessible memory follwing if XSIZE >= 4
          */
         bitmapBuffer[i] = ((unsigned char)(buf[y][x + 0]) << 7) |
                           ((unsigned char)(buf[y][x + 1]) << 6) |
                           ((unsigned char)(buf[y][x + 2]) << 5) |
                           ((unsigned char)(buf[y][x + 3]) << 4) |
                           ((unsigned char)(buf[y][x + 4]) << 3) |
                           ((unsigned char)(buf[y][x + 5]) << 2) |
                           ((unsigned char)(buf[y][x + 6]) << 1) |
                           ((unsigned char)(buf[y][x + 7]) << 0);
      }
   }

   bitOrder();
   bitmapDisplay();   
}


/* --------------------- CA simulation -------------------------------- */

/* random starting configuration */
static void initConfig(Line *buf, int lines)
{  int x, y;
#ifdef STRIPETEST
 /* simple stripes that should form a "jumping fixpoint" */
 for (y = 1;  y <= lines;  y++) {
    for (x = 1;  x <= XSIZE;  x++) {
       buf[y][x] = 0;
       for(i = 30; i >= 0; i-=2)
          buf[y][x] |= (unsigned int)(((y)%2) << i);
    }
 }
#else
/* Zufallsgeneratorinitialisierung mittels seed (Praktikumsbib) */
    initParallelRandomLEcuyer(424243, myRank, numProcs);
   for (y = 1;  y <= lines;  y++) {
      for (x = 1;  x <= XSIZE;  x++) {
         buf[y][x] = randInt(100) >= 50;
      }
   }
#endif
//        printf("%i: Initialization done.\n", myRank);
}

/* annealing rule from ChoDro96 page 34 
 * the table is used to map the number of nonzero
 * states in the neighborhood to the new state
 */
static State anneal[10] = {0, 0, 0, 0, 1, 0, 1, 1, 1, 1};
/* Fast majoritaet, ausser bei 4 1er: 1, 5 1er: 0 */
/* a: pointer to array; x,y: coordinates; result: n-th element of anneal,
      where n is the number of neighbors */
#define transition(a, x, y) \
   (anneal[(a)[(y)-1][(x)-1] + (a)[(y)][(x)-1] + (a)[(y)+1][(x)-1] +\
           (a)[(y)-1][(x)  ] + (a)[(y)][(x)  ] + (a)[(y)+1][(x)  ] +\
           (a)[(y)-1][(x)+1] + (a)[(y)][(x)+1] + (a)[(y)+1][(x)+1]])
  MPI_Request request[4];
MPI_Status stati[4];
/* treat torus like boundary conditions */
static void boundary(Line *buf, int lines)		/* Fuelle rand */
{  int x,y;
    MPI_Status status;
/* Bleibt auch in paralleler Version gleich */
   for (y = 1;  y <= lines;  y++) {
      /* copy rightmost column to the buffer column 0 */
      buf[y][0      ] = buf[y][XSIZE];

      /* copy leftmost column to the buffer column XSIZE + 1 */
      buf[y][XSIZE+1] = buf[y][1    ];
   }
/* Nachbarschaftskommunikation in 2 Schritten */
        int below = (myRank + numProcs + 1) % numProcs;
        int above = (myRank + numProcs - 1) % numProcs;
    
	MPI_Isend( buf[lines], XSIZE+2, MPI_CHAR, below, DOWNWARDS, MPI_COMM_WORLD, &request[0]);
	MPI_Isend( buf[1], XSIZE+2, MPI_CHAR, above, UPWARDS, MPI_COMM_WORLD, &request[1]);
        
	MPI_Irecv( buf[0], XSIZE+2, MPI_CHAR, above, DOWNWARDS, MPI_COMM_WORLD, &request[2]);
	MPI_Irecv( buf[lines+1], XSIZE+2, MPI_CHAR, below, UPWARDS, MPI_COMM_WORLD, &request[3]);
	
/*	MPI_Sendrecv(buf[lines], XSIZE+2, MPI_CHAR, below, DOWNWARDS, 
                     buf[0], XSIZE+2, MPI_CHAR, above, DOWNWARDS, 
                     MPI_COMM_WORLD, &status);
        MPI_Sendrecv(buf[1], XSIZE+2, MPI_CHAR, above, UPWARDS, 
                     buf[lines+1], XSIZE+2, MPI_CHAR, below, UPWARDS, 
                     MPI_COMM_WORLD, &status);
*/
}

/* make one simulation iteration with lines lines.
 * old configuration is in from, new one is written to to.
 */
static void simulate(Line *from, Line *to, int lines)
{
   int x,y;
   boundary(from, lines);	/* Rand aktualisieren */
   
   
   for (y = 2;  y <= lines-1;  y++) {
      for (x = 1;  x <= XSIZE;  x++) {
         to[y][x  ] = transition(from, x  , y);
      }
   } 
   MPI_Waitall( 4, request, stati);
  for (x = 1;  x <= XSIZE;  x++) {
         to[1][x  ] = transition(from, x  , 1);
         to[lines][x  ] = transition(from, x  , lines);
      }

}


/* --------------------- measurement ---------------------------------- */

int main(int argc, char** argv)
{  int lines, its;
   int i;
    double *timing;
   
   double startTime, nowTime, lastTime, pa; 
   Line *from, *to, *temp;
    Line *completeGrid;
   /* init */

   MPI_Init(&argc, &argv);

    MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
    MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
    
   if(argc != 5){
     fprintf(stderr,"\nSynopsis: caseq-slow <lines> <iterations> "
	     "<dispPeriod> <display>\n");
     fprintf(stderr,"argc=%d\n",argc);
     for(i=0;i<argc;i++){
     	fprintf(stderr,"argv[%d]=%s\n",i,argv[i]);
     }
     MPI_Abort(MPI_COMM_WORLD, 1);
   }

   lines = atoi(argv[1]);
    assert( (lines % numProcs) == 0);
    localLines = lines / numProcs;
   its   = atoi(argv[2]);
   displayPeriod = atoi(argv[3]);
   
/* Initialize local grids on all PEs */
    assert(from = malloc((localLines + 2) * sizeof(Line)));
    assert(to   = malloc((localLines + 2) * sizeof(Line)));
/* Initialize whole grid on the first PE */
    if(myRank == 0){
        assert( completeGrid = malloc((lines+2) * sizeof(Line)));
        timing = (double*) malloc(its * sizeof(double));
    }
   initConfig(from, localLines);
    if (displayPeriod && myRank == 0) bitmapInit(XSIZE, lines, argv[4], "BITMAP");  

   /* measurement loop */
   startTime = MPI_Wtime();   
   for (i = 0;  i < its;  i++) {
       /*/ Measure time /*/ 
       if(myRank == 0){
           lastTime = MPI_Wtime();
       }
     simulate(from, to, localLines);
       /*/ Measure time & store /*/
       if(myRank == 0){
           nowTime = MPI_Wtime();
           timing[i] = nowTime - lastTime;
           lastTime = nowTime;
       }
     temp = from;  from = to;  to = temp;
/* Gather op */
if(displayPeriod)       MPI_Gather( from[1], sizeof(Line)*localLines, MPI_CHAR, 
                  completeGrid[1], sizeof(Line)*localLines, MPI_CHAR, 
                  0, MPI_COMM_WORLD);      
        if (displayPeriod && myRank == 0)
            if (i % displayPeriod == 0) { 
                displayConfig(completeGrid, lines);
       }
   }
   
   printf("%fM cells per second %s\n",
         lines*XSIZE*its / (MPI_Wtime() - startTime)/1000,
         displayPeriod?"(but the states have been displayed)":"");
    if(myRank == 0){
        pa = pruned_average(timing, its, 0.25);
        printf("%g# time[us] %d trials\n",
               1e6 * pa,
              // 2.0 * length * sizeof(double) / pa / 1024.0 / 1024.0, 
               its);
    }
   if (displayPeriod && myRank == 0) {
      puts("Press q or n in display window to exit.");
      eventLoop();
   }

   closeAll();
   free(from);
   free(to);
    if(myRank==0)
        free(completeGrid);
   MPI_Finalize();
   return 0;
}
