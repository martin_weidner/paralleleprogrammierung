/* (c) 1996,1997 Peter Sanders, Ingo Boesnach */
/* simulate a cellular automaton (serial version)
 * periodic boundaries
 *
 * use graphical display
 * 
 * #1: Number of lines 
 * #2: Number of iterations to be simulated
 * #3: Length of period (in iterations) between displayed states. 
 *     0 means that no state is displayed.
 * #4: Name of X Display. E.g. "i90xxx.ira.uka.de:0.0"
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <mpi.h> 

#include "bitmap.h"
#include "random.h"

#include <assert.h>

/* horizontal size of the configuration */
#define XSIZE 512

/* "ADT" State and line of states (plus border) */
typedef char State;
typedef State Line[XSIZE + 2];
#define STATE MPI_CHAR
#define TOPLINETAG 42
#define BUTTOMLINETAG 47
/* every how many simulation cycles shall the display be updated ? */
int displayPeriod;

/* determine random integer between 0 and n-1 */
#define randInt(n) ((int)(nextRandomLEcuyer() * n))

/* --------------------- debugging and I/O ---------------------------- */

/* display configuration stored in buf which consists
 * of lines lines on a window using bitmap.h 
 */
static void displayConfig(Line *buf, int lines)
{  
	int i, x, y;

	/* pack data (-> 8 pixels/byte; necessary for bitmapDisplay) */
	i = 0;
	for (y = 1;  y <= lines;  y++) 
	{
		for (x = 1;  x <= XSIZE;  x+= 8, i++) 
		{
			/*>^ Dirty trick: this reads up to 7 elements beyond
			 *   a line boundary. This is OK because there is always
			 *   legally accessible memory follwing if XSIZE >= 4
			 */
			bitmapBuffer[i] = ((unsigned char)(buf[y][x + 0]) << 7) |
							  ((unsigned char)(buf[y][x + 1]) << 6) |
							  ((unsigned char)(buf[y][x + 2]) << 5) |
							  ((unsigned char)(buf[y][x + 3]) << 4) |
							  ((unsigned char)(buf[y][x + 4]) << 3) |
							  ((unsigned char)(buf[y][x + 5]) << 2) |
							  ((unsigned char)(buf[y][x + 6]) << 1) |
							  ((unsigned char)(buf[y][x + 7]) << 0);
			/* printf("%d ",bitmapBuffer[i]); */
		}
		/* printf("\n"); */
	}

	bitOrder();
	bitmapDisplay();   
}


/* --------------------- CA simulation -------------------------------- */

/* random starting configuration */
static void initConfig(Line *buf, int lines)
{  
	int x, y;
	int myId, numProcs;

#ifdef STRIPETEST
 /* simple stripes that should form a "jumping fixpoint" */
	for (y = 1;  y <= lines;  y++) {
		for (x = 1;  x <= XSIZE;  x++) {
			buf[y][x] = 0;
			for(i = 30; i >= 0; i-=2)
				buf[y][x] |= (unsigned int)(((y)%2) << i);
		}
	}
#else
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myId);
   
	initParallelRandomLEcuyer(47119, myId, numProcs);

	for (y = 1;  y <= lines;  y++) {
		for (x = 1;  x <= XSIZE;  x++) {
			buf[y][x] = randInt(100) >= 50;
		/*
		if(x>64 && x<=80)
			buf[y][x]=1;
		else
			buf[y][x]=0;
		 */
		}
	}
#endif
}

/* annealing rule from ChoDro96 page 34 
 * the table is used to map the number of nonzero
 * states in the neighborhood to the new state
 */
static State anneal[10] = {0, 0, 0, 0, 1, 0, 1, 1, 1, 1};
/* Fast majoritaet, ausser bei 4 1er: 1, 5 1er: 0 */
/* a: pointer to array; x,y: coordinates; result: n-th element of anneal,
      where n is the number of neighbors */
#define transition(a, x, y) \
   (anneal[(a)[(y)-1][(x)-1] + (a)[(y)][(x)-1] + (a)[(y)+1][(x)-1] +\
           (a)[(y)-1][(x)  ] + (a)[(y)][(x)  ] + (a)[(y)+1][(x)  ] +\
           (a)[(y)-1][(x)+1] + (a)[(y)][(x)+1] + (a)[(y)+1][(x)+1]])

/* treat torus like boundary conditions */
static void boundary(Line *buf, int lines)		/* Fuelle rand */
{  
	int x,y;
	int numProcs;
   
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
   
    for (y = 1;  y <= lines;  y++) {
		/* copy rightmost column to the buffer column 0 */
		buf[y][0      ] = buf[y][XSIZE];

		/* copy leftmost column to the buffer column XSIZE + 1 */
		buf[y][XSIZE+1] = buf[y][1    ];
    }
   
	if (numProcs == 1)
	{
		for (x = 0;  x <= XSIZE+1;  x++) {
			/* copy bottommost row to buffer row 0 */
			buf[0][x      ] = buf[lines][x];

			/* copy topmost row to buffer row lines + 1 */
			buf[lines+1][x] = buf[1][x    ];
		}
	}
}

/* make one simulation iteration with lines lines.
 * old configuration is in from, new one is written to to.
 */
static void simulate(Line *from, Line *to, int lines)
{
	int x,y;
  
	boundary(from, lines);		/* Rand aktualisieren */
	for (y = 1;  y <= lines;  y++) {
		for (x = 1;  x <= XSIZE;  x++) {
			to[y][x  ] = transition(from, x  , y);
		}
	}
}


/* --------------------- measurement ---------------------------------- */

int main(int argc, char** argv)
{  
	int lines, its;
	int i;
	double startTime; 
	Line *from, *to, *temp, *buf;
	int myId, numProcs;
	int simulatedLines = 0;
	MPI_Status status;

	/* init */

	MPI_Init(&argc, &argv);
   
	MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myId);

	if(argc != 5)
	{
		fprintf(stderr,"\nSynopsis: caseq-slow <lines> <iterations> "
			"<dispPeriod> <display>\n");
		fprintf(stderr,"argc=%d\n",argc);
		for(i=0;i<argc;i++)
		{
			fprintf(stderr,"argv[%d]=%s\n",i,argv[i]);
		}
		MPI_Abort(MPI_COMM_WORLD, 1);
	}

	lines = atoi(argv[1]);
	its   = atoi(argv[2]);
	displayPeriod = atoi(argv[3]);
   
	// Check if lines are multiple of PEs
	assert( lines%(numProcs) == 0 );
   
	if (numProcs != 1)
	{ // for more than one PE 
		//Number for each PE
		simulatedLines = lines / numProcs;
		//Each PE only gets the number of simulated lines + 2
		assert(from = malloc((simulatedLines + 2) * sizeof(Line)));
		assert(to   = malloc((simulatedLines + 2) * sizeof(Line)));
		assert(buf = malloc((lines + 2) * sizeof(Line)));
		initConfig(from+1, simulatedLines);
	}
	else
	{ // if there is only one PE
		assert(from = malloc((lines + 2) * sizeof(Line)));
		assert(to   = malloc((lines + 2) * sizeof(Line)));
		initConfig(from+1, lines);
		// necessary for simulate to save one branch
		simulatedLines = lines;
	}
      
	if ((displayPeriod) && (myId == 0))
	{   
		bitmapInit(XSIZE, lines, argv[4], "BITMAP");  
	}
   
	MPI_Barrier(MPI_COMM_WORLD);
   
	/* measurement loop */
	startTime = MPI_Wtime(); 

	for (i = 0;  i < its;  i++) 
	{
		//critical area (boundary area)
		if (numProcs != 1)
		{ // for more than one PE
			if( myId == 0 )
			{ // first PE (PE 0)
				MPI_Send(from[1], sizeof(Line), MPI_CHAR, numProcs-1, BUTTOMLINETAG, MPI_COMM_WORLD);
				MPI_Recv(from[0], sizeof(Line), MPI_CHAR, numProcs-1, TOPLINETAG, MPI_COMM_WORLD, &status);
				MPI_Send(from[simulatedLines], sizeof(Line), MPI_CHAR, 1, TOPLINETAG, MPI_COMM_WORLD);
				MPI_Recv(from[simulatedLines+1], sizeof(Line), MPI_CHAR, 1, BUTTOMLINETAG, MPI_COMM_WORLD, &status);
			}
			else if ( myId == numProcs-1 )
			{ // highest PE
				MPI_Recv(from[simulatedLines+1], sizeof(Line), MPI_CHAR, 0, BUTTOMLINETAG, MPI_COMM_WORLD, &status);
				MPI_Send(from[simulatedLines], sizeof(Line), MPI_CHAR, 0, TOPLINETAG, MPI_COMM_WORLD);
				MPI_Recv(from[0], sizeof(Line), MPI_CHAR, myId-1, TOPLINETAG, MPI_COMM_WORLD, &status);
				MPI_Send(from[1], sizeof(Line), MPI_CHAR, myId-1, BUTTOMLINETAG, MPI_COMM_WORLD);
			}
			else
			{  // for all other PE than PE 0 and highest PE
				if( myId%2 == 0)
				{ // even
					MPI_Send(from[1], sizeof(Line), MPI_CHAR, myId-1, BUTTOMLINETAG, MPI_COMM_WORLD);
					MPI_Recv(from[0], sizeof(Line), MPI_CHAR, myId-1, TOPLINETAG, MPI_COMM_WORLD, &status);
					MPI_Send(from[simulatedLines], sizeof(Line), MPI_CHAR, myId+1, TOPLINETAG, MPI_COMM_WORLD);
					MPI_Recv(from[simulatedLines+1], sizeof(Line), MPI_CHAR, myId+1, BUTTOMLINETAG, MPI_COMM_WORLD, &status);
				}
				else  
				{ //odd
					MPI_Recv(from[0], sizeof(Line), MPI_CHAR, myId-1, TOPLINETAG, MPI_COMM_WORLD, &status);
					MPI_Send(from[1], sizeof(Line), MPI_CHAR, myId-1, BUTTOMLINETAG, MPI_COMM_WORLD);
					MPI_Send(from[simulatedLines], sizeof(Line), MPI_CHAR, myId+1, TOPLINETAG, MPI_COMM_WORLD);
					MPI_Recv(from[simulatedLines+1], sizeof(Line), MPI_CHAR, myId+1, BUTTOMLINETAG, MPI_COMM_WORLD, &status);
				}
			}
		}
		
		simulate(from, to, simulatedLines);
	 
		temp = from;  
		from = to;  
		to = temp;
	
		if (numProcs != 1)
		{ // to bring all results together from each PE the root is here PE 0
			MPI_Gather(from[1], simulatedLines*sizeof(Line), MPI_CHAR, buf[1], simulatedLines*sizeof(Line), MPI_CHAR, 0, MPI_COMM_WORLD);
		}
	 
		if ((displayPeriod) && (myId == 0))
		{ // PE 0 is responsible for the display
			if (i % displayPeriod == 0) 
			{ 
				if(numProcs != 1)
				{
					displayConfig(buf, lines);
				}
				else
				{
					displayConfig(from, lines);
				}
				/*
				puts("Press q or n for next.");
				eventLoop();
				*/
			}
		}
	}
   
	printf("%f cells per second %s\n",
		lines*XSIZE*its / (MPI_Wtime() - startTime),
			displayPeriod?"(but the states have been displayed)":"");

	if ((displayPeriod) && (myId == 0)) 
	{
		puts("Press q or n in display window to exit.");
		eventLoop();
	}

	closeAll();
	free(from);
	free(to);
	if (numProcs != 1)
	{ // because buf does not exist for one PE
		free(buf);
	}

	MPI_Finalize();
	return 0;
}
