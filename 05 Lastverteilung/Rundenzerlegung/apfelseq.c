/* (c) 1997 Ingo Boesnach */
/* mandelbrot set (serial version)
 * argv[1]: smallest real part considered
 * argv[2]: smallest imaginary part considered
 * argv[3]: extent of square area considered
 * argv[4]: resolution 
 * argv[5]: maximum number of interations
 * argv[6]: iterations 
 * e.g. "poe a.out -procs 2 -1.5 -1 2 400 200 i90s14.ira.uka.de:0.0"
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <assert.h>
#include "mpi.h"
#include "random.h"
#include "bitmap.h"
#include "timing.h"

#define DISPLAY   0
#define debug printf

#define komplex double _Complex



int iProc, nProc;


typedef struct {int start;  int end; } Work;

/*********** Mandelbrot specific code ******************************/

#define LARGE   2.0

komplex z0;
double extent;  /* a_0 in the book         */
int resolution; /* n in the book           */
double step;    /* a_0/n                   */
int maxiter;    /* m_max in the book       */
int withgraph;  /* Flag: display results ? */

komplex _complex(double i, double j){
    return i + j * _Complex_I;
}

/* perform a single Mandelbrot iteration starting at c and return
 * number of iterations
 */
int iterate(int pos, int peId)
{  
    int iter;
    komplex c = z0 + _complex((double)(pos % resolution) * step, 
               (double)(pos / resolution) * step);
    komplex z = c;
    for (iter = 1;  iter < maxiter && abs(z) <= LARGE;  iter++) z = z*z + c;
    return iter;
}
 

/*********** Worker Code **********************/

/* work on an interval of integers representing candidate elements
 * of the Mandelbrot set.
 */
void worker(Work work, int peId, int numProcs)
{ 
    int elem;
    int *result;
    /* contains interval size and a sequence of integers encoding one element of 
    * the Mandelbrot set each 
    */
    result = (int*)malloc(resolution * resolution * sizeof(int) + 1);
    assert(result != 0);
  
    elem = 1;
    result[0] = work.start; /* remember where this interval started */
    for (int ptr = peId; ptr < resolution*resolution; ptr += numProcs) {
        if ( iterate(work.start, peId) == maxiter ) {
            /* we assume to have an element of the Mandelbrot set here */
            result[elem] = work.start;
            elem++;
        }
    }
    /* send results to server */
    result[0] = work.end - result[0] + 1;
    //MPI_Send(result, elem, MPI_INT, 0, DISPLAY, MPI_COMM_WORLD);  

    free(result);
}


/*********************************************************************/

int main(int argc, char **argv)
{
    Work work;
    double *timing, startTime, stopTime;
    int its;
  
    MPI_Init(&argc, &argv);
    
    assert(argc >= 6);
    assert(argc <= 7);
    z0         = _complex(atof(argv[1]), atof(argv[2]));
    extent     = atof(argv[3]);
    resolution = atoi(argv[4]);
    step = extent / resolution;
    maxiter = atoi(argv[5]);
    its = atoi(argv[6]);
  
    MPI_Comm_size(MPI_COMM_WORLD, &nProc);
    MPI_Comm_rank(MPI_COMM_WORLD, &iProc);
    
    if(!iProc) {
        assert( timing = malloc(its * sizeof(double)) );
    }
  
    /*/ PE i bekommt streifen: [(n*n)/P * i, (n*n)/P * (i+1) - 1]  /*/
    int n = resolution;
    int lower = (n*n / nProc) * iProc;
    int upper = (n*n / nProc) * (iProc +1) - 1;
    for (int r = 0; r<its; r++) {
        startTime = MPI_Wtime();

        work.start = 0;
        work.end = n*n - 1;
        worker(work, iProc, nProc);
        MPI_Barrier(MPI_COMM_WORLD);
	stopTime = MPI_Wtime() - startTime;
        MPI_Reduce( &stopTime, &timing[r], 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD );
    }
    
    if(!iProc){
        double pa = pruned_average(timing, its, 0.25);
        printf("%i\t %f\t %i\t %i #PEs\t #Time[us]\t #n*n\t  #Iterations\n",nProc, 1e6*stopTime, n*n, its); 
		fflush(stdout);
        free(timing);
    }

    
    MPI_Finalize();
  return 0;
}

