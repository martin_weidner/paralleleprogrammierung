/* (c) 1997 Ingo Boesnach */
/* mandelbrot set (serial version)
 * argv[1]: smallest real part considered
 * argv[2]: smallest imaginary part considered
 * argv[3]: extent of square area considered
 * argv[4]: resolution 
 * argv[5]: maximum number of interations
 * argv[6]: iterations 
 * argv[7]: k
 * e.g. "poe a.out -procs 2 -1.5 -1 2 400 200 i90s14.ira.uka.de:0.0"
 */ 

#include <stdio.h>
#include <stdlib.h>
#include <complex.h>
#include <string.h>
#include <assert.h>
#include "mpi.h"
#include "random.h"
#include "bitmap.h"
#include "timing.h"
#include "sort.h"

#define DISPLAY   0
#define debug printf

#define komplex double _Complex

#define WORKTAG 1
#define DIETAG 2


int iProc, nProc;


typedef struct {int start;  int end; } Work;

/*********** Mandelbrot specific code ******************************/

#define LARGE   2.0

komplex z0;
double extent;  /* a_0 in the book         */
int resolution; /* n in the book           */
double step;    /* a_0/n                   */
int maxiter;    /* m_max in the book       */
int withgraph;  /* Flag: display results ? */

komplex _complex(double i, double j){
    return i + j * _Complex_I;
}

/* perform a single Mandelbrot iteration starting at c and return
 * number of iterations
 */
int iterate(int pos)
{  
    int iter;
    komplex c = z0 + _complex((double)(pos % resolution) * step, 
                              (double)(pos / resolution) * step);
    komplex z = c;
    for (iter = 1;  iter < maxiter && abs(z) <= LARGE;  iter++) z = z*z + c;
    return iter;
}


/*********** Worker Code **********************/

/* work on an interval of integers representing candidate elements
 * of the Mandelbrot set.
 */
void worker( int peId, int packageSize )
{ 
    int elem;
    int *result, *workPackage, *grenzen;
    /* contains interval size and a sequence of integers encoding one element of 
     * the Mandelbrot set each 
     */
    result = (int*)malloc(packageSize * sizeof(int) + 1 );
    assert(result != 0);
    assert( grenzen = malloc(2*sizeof(int)) );
    assert( workPackage = malloc(sizeof(int) * packageSize) );
    
    MPI_Status status;
    
    /*/ Initiales Arbeitspaket empfangen /*/
    MPI_Recv( grenzen, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
    
    while(1){
        elem = 0;
        if (status.MPI_TAG == DIETAG) {
            break;
        }
//        printf("%i: starting work on %d to %d\n", peId, grenzen[0], grenzen[1]);        
        for( int i = grenzen[0]; i<grenzen[1]; i++ ){
            if ( iterate(i) == maxiter) {
                result[elem] = i;
                elem++;
            }
        }
        MPI_Send(result, elem, MPI_INT, 0, 0, MPI_COMM_WORLD);
        MPI_Recv(grenzen, 2, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
    }
    MPI_Send(result, elem, MPI_INT, 0, DISPLAY, MPI_COMM_WORLD);  
    free( grenzen );
    free( result  );
}

void get_work_package(int packageSize, int *grenzen, int *ptr){
    grenzen[0] = *ptr;
    *ptr += packageSize;
    if(*ptr >= resolution*resolution){
        *ptr = resolution*resolution;
    }
    grenzen[1] = *ptr-1;
}

void master(int numProcs, int packageSize)
{
    int workPtr = 0, *grenzen, *result, *array_of_indices, outcount;
    MPI_Status status, *array_of_status;
    MPI_Request *array_of_requests;
    int *result_array, result_ptr=0, length;
    
    assert( grenzen = malloc(2*sizeof(int)) );
    assert( array_of_status = malloc((numProcs-1) * sizeof(MPI_Status) ));
    assert( array_of_indices = malloc((numProcs-1) * sizeof(int) ));
    assert( array_of_requests = malloc((numProcs-1) * sizeof(MPI_Request) ));
    assert( result_array = malloc(resolution*resolution*sizeof(int)) );
    assert( result = malloc(resolution*resolution*sizeof(int)) );
    
    memset(result, 0, (resolution*resolution)*sizeof(int));
    
    for (int rank=1; rank<numProcs; rank++) {
        if(workPtr < (resolution * resolution) ){
            int r_pos = workPtr;
            get_work_package(packageSize, grenzen, &workPtr);
            MPI_Send( grenzen, 2, MPI_INT, rank, WORKTAG, MPI_COMM_WORLD );
            MPI_Irecv( &result[r_pos], packageSize, MPI_INT, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &array_of_requests[rank] );
        }
    }
    
    while ( workPtr < (resolution * resolution) ) {
        MPI_Waitsome(numProcs-1, array_of_requests, &outcount, array_of_indices, array_of_status);
        for (int i=0; i<outcount; i++) {
            MPI_Get_count(&array_of_status[i], MPI_INT, &length);
            result_ptr += length;
            if(workPtr < (resolution * resolution)){
                int r_pos = workPtr;
                get_work_package(packageSize, grenzen, &workPtr);
                MPI_Send(grenzen, 2, MPI_INT, array_of_indices[i], WORKTAG, MPI_COMM_WORLD );
                MPI_Irecv(&result[r_pos], packageSize, MPI_INT, array_of_indices[i], MPI_ANY_TAG, MPI_COMM_WORLD, &array_of_requests[array_of_indices[i] ]);
            }
        }
    }
    /*/ Finishing Sequence /*/ 
    for (int rank=1; rank < numProcs; rank++) {
//        MPI_Recv( result, packageSize, MPI_INT, rank, MPI_ANY_TAG, MPI_COMM_WORLD, &status );
        MPI_Send(0, 0, MPI_INT, rank, DIETAG, MPI_COMM_WORLD);
    }
    free( grenzen );
    free(array_of_status);
    free(array_of_indices);
    free(result_array);
    free(result);
    MPI_Request_free(array_of_requests);
}

/*********************************************************************/

int main(int argc, char **argv)
{
    Work work;
    double *timing, startTime, stopTime;
    int its, k;
    int packageSize;
    
    MPI_Init(&argc, &argv);

    assert(argc >= 6);
    assert(argc <= 8);
    z0         = _complex(atof(argv[1]), atof(argv[2]));
    extent     = atof(argv[3]);
    resolution = atoi(argv[4]);
    step = extent / resolution;
    maxiter = atoi(argv[5]);
    its = atoi(argv[6]);
    k = atoi(argv[7]);

    packageSize = resolution * resolution / k;
    if(packageSize < 1) {
        packageSize = 1;
    }
    
    MPI_Comm_size(MPI_COMM_WORLD, &nProc);
    MPI_Comm_rank(MPI_COMM_WORLD, &iProc);
    
    if(!iProc){
        assert( timing = malloc(its * sizeof(int)) );
    }
       
    /*/ PE i bekommt streifen: [(n*n)/P * i, (n*n)/P * (i+1) - 1]  /*/
    for (int r = 0; r<its; r++) {
        startTime = MPI_Wtime();
        if(!iProc){
            master( nProc, packageSize );
        } else {
            worker( iProc, packageSize );
        }
        stopTime = MPI_Wtime() - startTime;
        MPI_Reduce( &stopTime, &timing[r], 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD );
    }
    
    if(!iProc){
        double pa = pruned_average(timing, its, 0.25);
        printf("%i\t %f\t %i\t %i\t %i #PEs\t Time[us]\t n*n\t  Iterations\t n*n/k\n",nProc, 1e6*stopTime, resolution*resolution, its, packageSize); 
		fflush(stdout);
        free(timing);
    }
    
    MPI_Finalize();
    return 0;
}

