/* pingpong test with timing vor varying message sizes 
 * June 4 97: version with pruned_average
 *
 * (c) 1996,1997 Peter Sanders
 */
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "timing.h"

/* A small debugging aid. Test condition "c" and print an error if it fails */
#define Assert(c) if(!(c)){printf(\
   "\nAssertion violation %s:%u:" #c "\n", __FILE__, __LINE__);}

/* Tags */

#define PING 42
#define PONG 17

/* buffer for message exchange */
#define BUFFERSIZE 1024 * 1024
double buffer[BUFFERSIZE];

/* each measurement should take about 1s */
#define NTrials(length) (1.0 / (50e-6 + sizeof(double) * (length) * 1e-8))

int main(int argc, char** argv)
{  int myId, numProcs, i, j, length, trials;
   double lastTime, nowTime, pa, **timing;
   MPI_Status status;

   MPI_Init(&argc, &argv);

   length = 1;

   MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
   MPI_Comm_rank(MPI_COMM_WORLD, &myId);
 //  Assert(numProcs == 9);
   timing = (double**) malloc(numProcs*sizeof(double*));
for(j=numProcs-1;j>0;j--){
   timing[j] = (double*) malloc(NTrials(1) * sizeof(double));
}
   Assert(timing != 0);
   for (length = 1;  length <= BUFFERSIZE;  length *= 2) {
      trials = NTrials(length);
	MPI_Barrier(MPI_COMM_WORLD);
      if (myId == 0) {
         lastTime = MPI_Wtime();
         for (i = 0;  i < trials;  i++) {
for(j=numProcs-1;j>0;j--){
            MPI_Send(buffer, length, MPI_DOUBLE, j, PING, MPI_COMM_WORLD);
            MPI_Recv(buffer, length, MPI_DOUBLE, j, PONG, MPI_COMM_WORLD, &status);
            nowTime = MPI_Wtime();
            timing[j][i] = nowTime - lastTime;
            lastTime = nowTime;
}
         }
      } else {
         for (i = 0;  i < trials;  i++) {
            MPI_Recv(buffer, length, MPI_DOUBLE, 0, PING, MPI_COMM_WORLD, &status);
            MPI_Send(buffer, length, MPI_DOUBLE, 0, PONG, MPI_COMM_WORLD);
         }
      }
         
      if (myId == 0) {
	for(j=numProcs-1;j>0;j--){
         pa = pruned_average(timing[j], trials, 0.25);
         printf("%ld %g %g # length[B], time[us], bandwidth [MiB/s] %d trials, PE: %d\n",
                length * sizeof(double), 1e6 * pa / 2.0,
                2.0 * length * sizeof(double) / pa / 1024.0 / 1024.0, 
                trials, j);
      }}
   }
//}
   MPI_Finalize(); 
   return 1;
}      
         
