/* pingpong test with timing vor varying message sizes
 * June 4 97: version with pruned_average
 *
 * (c) 1996,1997 Peter Sanders
 */
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include "timing.h"
#include <assert.h>
/* A small debugging aid. Test condition "c" and print an error if it fails */
#define Assert(c) if(!(c)){printf(\
   "\nAssertion violation %s:%u:" #c "\n", __FILE__, __LINE__);}

/* Tags */

#define PING 42
#define PONG 17

/* buffer for message exchange */
#define BUFFERSIZE 1024 * 1024
double buffer[BUFFERSIZE];

/* each measurement should take about 1s */
#define NTrials(length) (1.0 / (50e-6 + sizeof(double) * (length) * 1e-8))

int main(int argc, char** argv)
{  int myId, numProcs, i, length, trials;
   double lastTime, nowTime, pa, **timing;
   MPI_Status status;

   MPI_Init(&argc, &argv);

   length = 1;

   MPI_Comm_size(MPI_COMM_WORLD, &numProcs);
   MPI_Comm_rank(MPI_COMM_WORLD, &myId);
//  Assert(numProcs == 2);

   // define an array for the number of processes(each entry has a size of double), i.e. 2-dim array
   timing = (double**) malloc(numProcs*sizeof(double*));
   Assert(timing != 0);

   for ( int procsCounter = 1; procsCounter < numProcs; procsCounter++ )
   {
      timing[procsCounter] = (double*) malloc(NTrials(1) * sizeof(double));
      Assert(timing != 0);
   }
