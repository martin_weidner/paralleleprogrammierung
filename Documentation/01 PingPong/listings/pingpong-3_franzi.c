[...]
   double lastTime, nowTime, pa, **timing;
[...]
   // define an array for the number of PE, i.e. 2-dim array
   timing = (double**) malloc(numProcs*sizeof(double*));
   Assert(timing != 0); 

   // allocate the second dimension
   for ( int procsCounter = 1; procsCounter < numProcs; procsCounter++ )
   {
      timing[procsCounter] = (double*) malloc(NTrials(1) * sizeof(double));
      Assert(timing != 0); 
   }
   for (length = 1;  length <= BUFFERSIZE;  length *= 2) {
      trials = NTrials(length);
      MPI_Barrier(MPI_COMM_WORLD);
      if (myId == 0) {
         lastTime = MPI_Wtime();
         for (i = 0;  i < trials;  i++) {
            for (int procsCounter = 1; procsCounter < numProcs; procsCounter++ )
            {
               MPI_Send(buffer, length, MPI_DOUBLE, procsCounter, PING, MPI_COMM_WORLD);
               MPI_Recv(buffer, length, MPI_DOUBLE, procsCounter, PONG, MPI_COMM_WORLD, &status);
               nowTime = MPI_Wtime();
               timing[procsCounter][i] = nowTime - lastTime;
               lastTime = nowTime;
            }
         }
      } else {
         for (i = 0;  i < trials;  i++) {
            MPI_Recv(buffer, length, MPI_DOUBLE, 0, PING, MPI_COMM_WORLD, &status);
            MPI_Send(buffer, length, MPI_DOUBLE, 0, PONG, MPI_COMM_WORLD);
         }
      }
      if (myId == 0) {
         for (int procsCounter = 1; procsCounter < numProcs; procsCounter++ )
         {
             pa = pruned_average(timing[procsCounter], trials, 0.25);
             printf("%ld %g %g # length[B], time[us], bandwidth [MiB/s]; %d trials; %i PE \n",
                length * sizeof(double), 1e6 * pa / 2.0,
                2.0 * length * sizeof(double) / pa / 1024.0 / 1024.0,
                trials, procsCounter);
         }
      }
   }
[...]
