[...]
   timing = (double**) malloc(numProcs*sizeof(double*));
for(j=numProcs-1;j>0;j--){
   timing[j] = (double*) malloc(NTrials(1) * sizeof(double));}
      [...]
      if (myId == 0) {    [...] for (i = 0;  i < trials;  i++) {
for(j=numProcs-1;j>0;j--){
            MPI_Send(buffer, length, MPI_DOUBLE, j, PING, MPI_COMM_WORLD);
            MPI_Recv(buffer, length, MPI_DOUBLE, j, PONG, MPI_COMM_WORLD, &status); [...]
}}} else { for (i = 0;  i < trials;  i++) {
            [...] }} 
      if (myId == 0) {
	for(j=numProcs-1;j>0;j--){    [...]
         printf("%ld %g %g # length[B], time[us], bandwidth [MiB/s] %d trials, PE: %d\n",
                length * sizeof(double), 1e6 * pa / 2.0,
                2.0 * length * sizeof(double) / pa / 1024.0 / 1024.0, 
                trials, j);
      }}}[...]