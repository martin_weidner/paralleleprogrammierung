int below = (myRank + numProcs + 1) % numProcs;
int above = (myRank + numProcs - 1) % numProcs;

MPI_Sendrecv(buf[lines], XSIZE+2, MPI_CHAR, below, DOWNWARDS, 
			buf[0], XSIZE+2, MPI_CHAR, above, DOWNWARDS, 
			MPI_COMM_WORLD, &status);
MPI_Sendrecv(buf[1], XSIZE+2, MPI_CHAR, above, UPWARDS, 
			buf[lines+1], XSIZE+2, MPI_CHAR, below, UPWARDS, 
			MPI_COMM_WORLD, &status);