MPI_Request request[4];
MPI_Status stati[4];

MPI_Isend( buf[lines], XSIZE+2, MPI_CHAR, below, DOWNWARDS, MPI_COMM_WORLD, &request[0]);
MPI_Isend( buf[1], XSIZE+2, MPI_CHAR, above, UPWARDS, MPI_COMM_WORLD, &request[1]);    
MPI_Irecv( buf[0], XSIZE+2, MPI_CHAR, above, DOWNWARDS, MPI_COMM_WORLD, &request[2]);
MPI_Irecv( buf[lines+1], XSIZE+2, MPI_CHAR, below, UPWARDS, MPI_COMM_WORLD, &request[3]); 
MPI_Waitall( 4, request, stati);  