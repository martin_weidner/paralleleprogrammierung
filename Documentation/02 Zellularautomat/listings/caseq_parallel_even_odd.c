[...]     
	 //critical are ( boundary area)
	if (numProcs != 1)
		{ // for more than one PE
			if( myId == 0 )
			{ // first PE (PE 0)
				MPI_Send(from[1], sizeof(Line), MPI_CHAR, numProcs-1, BUTTOMLINETAG, MPI_COMM_WORLD);
				MPI_Recv(from[0], sizeof(Line), MPI_CHAR, numProcs-1, TOPLINETAG, MPI_COMM_WORLD, &status);
				MPI_Send(from[simulatedLines], sizeof(Line), MPI_CHAR, 1, TOPLINETAG, MPI_COMM_WORLD);
				MPI_Recv(from[simulatedLines+1], sizeof(Line), MPI_CHAR, 1, BUTTOMLINETAG, MPI_COMM_WORLD, &status);
			}
			else if ( myId == numProcs-1 )
			{ // highest PE
				MPI_Recv(from[simulatedLines+1], sizeof(Line), MPI_CHAR, 0, BUTTOMLINETAG, MPI_COMM_WORLD, &status);
				MPI_Send(from[simulatedLines], sizeof(Line), MPI_CHAR, 0, TOPLINETAG, MPI_COMM_WORLD);
				MPI_Recv(from[0], sizeof(Line), MPI_CHAR, myId-1, TOPLINETAG, MPI_COMM_WORLD, &status);
				MPI_Send(from[1], sizeof(Line), MPI_CHAR, myId-1, BUTTOMLINETAG, MPI_COMM_WORLD);
			}
			else
			{  // for all other PE than PE 0 and highest PE
				if( myId%2 == 0)
				{ // even
					MPI_Send(from[1], sizeof(Line), MPI_CHAR, myId-1, BUTTOMLINETAG, MPI_COMM_WORLD);
					MPI_Recv(from[0], sizeof(Line), MPI_CHAR, myId-1, TOPLINETAG, MPI_COMM_WORLD, &status);
					MPI_Send(from[simulatedLines], sizeof(Line), MPI_CHAR, myId+1, TOPLINETAG, MPI_COMM_WORLD);
					MPI_Recv(from[simulatedLines+1], sizeof(Line), MPI_CHAR, myId+1, BUTTOMLINETAG, MPI_COMM_WORLD, &status);
				}
				else  
				{ //odd
					MPI_Recv(from[0], sizeof(Line), MPI_CHAR, myId-1, TOPLINETAG, MPI_COMM_WORLD, &status);
					MPI_Send(from[1], sizeof(Line), MPI_CHAR, myId-1, BUTTOMLINETAG, MPI_COMM_WORLD);
					MPI_Send(from[simulatedLines], sizeof(Line), MPI_CHAR, myId+1, TOPLINETAG, MPI_COMM_WORLD);
					MPI_Recv(from[simulatedLines+1], sizeof(Line), MPI_CHAR, myId+1, BUTTOMLINETAG, MPI_COMM_WORLD, &status);
				}
			}
		}
[...]