  boundary(from, lines);	/* Rand aktualisieren */ 
  for (y = 2;  y <= lines-1;  y++) {
      for (x = 1;  x <= XSIZE;  x++) {
         to[y][x  ] = transition(from, x  , y);
      }
  } 
  MPI_Waitall( 4, request, stati);
  for (x = 1;  x <= XSIZE;  x++) {
         to[1][x] = transition(from, x, 1);
         to[lines][x] = transition(from, x, lines);
  }