static void neighbourBoundary(Line *buf, int lines)
{
    int below = (myRank + numProcs + 1) % numProcs;
    int above = (myRank + numProcs - 1) % numProcs;
    
    MPI_Isend( buf[lines], (XSIZE+2)*(K_LINES), MPI_CHAR, below, DOWNWARDS, MPI_COMM_WORLD, &request[0]);
    MPI_Isend( buf[K_LINES], (XSIZE+2)*(K_LINES), MPI_CHAR, above, UPWARDS, MPI_COMM_WORLD, &request[1]);
    
    MPI_Irecv( buf[0], (XSIZE+2)*(K_LINES), MPI_CHAR, above, DOWNWARDS, MPI_COMM_WORLD, &request[2]);
    MPI_Irecv( buf[lines+K_LINES], (XSIZE+2)*(K_LINES), MPI_CHAR, below, UPWARDS, MPI_COMM_WORLD, &request[3]);
}

static void simulate(Line *from, Line *to, int lines)
{
   int x,y;
   boundary(from, lines); /* update boundary */
     
   for (y = 1; y <= lines; y++) {
      for (x = 1; x <= XSIZE; x++) {
         to[y][x] = transition(from, x , y);
      }
   }
}

int main(int argc, char** argv)
{
[...]
	assert(from = malloc((localLines + 2*K_LINES) * sizeof(Line)));
	[...]
	
	simulate(from+K_LINES+1, to+K_LINES+1, localLines-2);
	MPI_Waitall( 4, request, stati);
	simulate(from, to, K_LINES);
	simulate(from+K_LINES+localLines-1, to+K_LINES+localLines-1, K_LINES);

	for(counter=1; counter < K_LINES; counter++)
	{	[...]
		boundary(from, localLines);
		neighbourBoundary(from, localLines);
		simulate(from+counter, to+counter, localLines+(2*K_LINES)-2-(counter*2));
		temp = from; from = to; to = temp;
		[...]
	}
}
		