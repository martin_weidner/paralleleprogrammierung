   [...]
   // Check if lines are multiple of PEs
   if ( lines%numProcs != 0 )
   {
	  perror("Number of lines is not a multiple of the number of PEs!");
	  exit 1;
   }
  [...]