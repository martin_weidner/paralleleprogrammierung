%!TEX root = ../PraktikumMain.tex
\chapter{Lastverteilung}
\label{cha:aufgabe5}
Bisher haben wir die Präfixsumme als Lastverteilungsalgorithmus kennengelernt, die für einige Algorithmen, wie Quicksort, verwendbar sind. Im Folgenden, werden weitere statische und dynamische Lastverteilungsalgorithmen präsentiert.\par
Für die vorgestellten Lastverteilungsalgorithmen wird das Problem der Approximation der Mandelbrotmenge verwendet. Die Einzelberechnungen der Gesamtlösung sind dabei unvorhersehbar. \\
Zur Approximation wird für komplexe Zahlen $c$ eine festgelegte Anzahl $k$ von Berechnungen durchgeführt: $z_c(0):=0$, $z_c(m+1) = z_c(m)^2 + c:m\le k$. Falls sich $z_c$ nach $k$ Iterationen unter einer bestimmten Schranke befindet, so wird angenommen, dass $c$ ein Teil der Mandelbrotmenge ist, da mit $k$ Runden nicht gezeigt werden konnte, dass es nicht drin liegt. \\
Bei genauerer Betrachtung zeichnet sich ab, dass anscheinend die Berechnung für einige $c$ einfach ist, da sie nach wenigen Iterationen die Schranke überschreiten. Für andere $c$ allerdings sind wahrscheinlich $k$ Iterationen notwendig.\\
Zur effizienten, parallelen Lösung dieses Algorithmus ist daher eine geschickte Lastbalancierung notwendig, damit alle PEs etwa gleich stark und gleich lang ausgelastet sind.\par
Das initiale Programm wurde mit zwei verschiedenen Konfigurationen gestartet, das Ergebnis ist in Abbildung~\ref{fig:05:init} zu sehen. Leicht zu erkennen ist der Grund, warum die Lastverteilung für Abbildung~\ref{fig:05:b} schwieriger ist: In dieser Konfiguration befinden sich die schweren Probleminstanzen - lokal betrachtet - am rechten, unteren Rand, sodass es zu einem Häufungspunkt mit viel Arbeit und einem im Vergleich riesigen Anteil mit wenig Arbeit kommt. Entscheidend für einen guten Lastverteilungsalgorithmus ist das geschickte Aufteilen dieses Bereichts, der viel Arbeit impliziert.
\begin{figure}[!ht]     
		\centering  
		\subfloat[$z_0 = -1.5 - i, a_0 = 2$]{
			\includegraphics[width=0.4\textwidth]{images/problem1.png}
			\label{fig:05:init:a}}% 
		\qquad
		\subfloat[$z_0 = -0.5 + 0.82i, a_0 = 0.45$]{
			\includegraphics[width=0.4\textwidth]{images/problem2.png}
			\label{fig:05:b}}%
		\caption{Berechnete Mandelbrotmenge mit zwei verschiedenen Konfigurationen}
		\label{fig:05:init}
	\end{figure}

Im Folgenden, werden verschiedene Lastverteilungsverfahren vorgestellt und mit Messungen anhand der Approximation der Mandelbrotmenge bewertet.
\section{Statische Lastverteilung} % (fold)
\label{sec:statischeLastverteilung}

\subsection{Streifenzerlegung}
\label{sub:streifenZerlegung}
Bei diesem Algorithmus findet eine streifenweise Zerlegung des Gitters der $n \times n$ Elemente statt. Dabei werden von jedem PE $i$ die Zahlen auf dem Intervall:  $[ l .. u ] $ berechnet, wobei $l=\frac{n^2}{P}\cdot i$ und $u=\frac{n^2}{P}\cdot (i+1) - 1$ gilt. Es ist ersichtlich, dass kein Koordinationsaufwand betrieben werden muss, weil jedes PE nur seinen eigenen Rang $i$, die Gesamtanzahl der PEs $P$ und die Gesamtgröße des Problems $N = n^2$ kennt.\\
Die Messung wurde mit der folgenden Konfiguration gestartet: \emph{xcbatch X apfelseq -0.5 0.82 0.45 256 500 500}, $X \in \{1,4\}$. Dabei kam für $P=1$ eine Laufzeit von ca. $6900 \mu s$ heraus, bei $P=4$ eine Laufzeit von ca. $4400 \mu s$ (vgl. Abbildung~\ref{fig:05:streifenweise}). Das Lastverteilungsverfahren liefert keine befriedigende Verteilung der Arbeit, da bei $4$-facher PE-Anzahl die Laufzeit gerade um etwa ein Drittel sinkt.
	\begin{figure}[!ht]     
		\centering  
		\includegraphics[width=0.5\textwidth]{images/streifenweise.pdf}
		\caption{Laufzeit bei streifenweiser Zerlegung (links: 1PE, rechts: 4PEs, y-Achse in $\mu$s)}  
		\label{fig:05:streifenweise}
	\end{figure}


\subsection{Scatter-Decomposition}
\label{sub:scatterDecomposition}
Der Ansatz dieses Algorithmus folgt einer rundenweisen Zerlegung der Probleminstanz, wobei das Teilproblem $k$ vom Prozessor $i \mod P$ berechnet wird. Der Algorithmus wurde auf unsere Probleminstanz entsprechend adaptiert, sodass innerhalb einer For-Schleife auf PE $i$ der Kopf folgendermaßen gesteuert wird: $a=i$; $a<n^2$; $a+\!\!\!=P$.
Dieser Lastverteilungsansatz wird auch einer Messung unterzogen, die in Abbildung~\ref{fig:05:rundenweise} zu sehen ist (gleiche Eingabeparameter wie zuvor). Durch eine rundenweise Zerlegung des Problems gelang ein Speedup von vier bei $P=4$, sodass die Last auf alle verfügbaren PEs gleichmäßig aufgeteilt wurde.
	\begin{figure}[!ht]     
		\centering
		\includegraphics[width=0.9\textwidth]{images/rundenweise}
		\caption{Laufzeit Scatter-Decomposition vs. Rundenaufteilung \footnotesize{(y-Achse in $\mu$s)}}  
		\label{fig:05:rundenweise}
	\end{figure}

\section{Master-Worker-Schema}
\label{sec:masterWorkerSchema}
Das Master-Worker-Schema stellt einen Ansatz zur dynamischen Lastverteilung bereit. Hierbei repräsentiert ein PE den Master, der für die Verteilung der Probleminstanzen und zum Einsammeln der Berechnungsergebnisse zuständig ist. Die Worker erfragen Arbeit beim Master, führen Berechnungen auf den Teilproblemen durch und senden die Ergebnisse dem Master.\par
Das Schema wurde für das Mandelbrotproblem implementiert, sodass PE $0$ für die Verteilung der Aufgaben zuständig ist. Alle weiteren PEs führen Berechnungen auf den zugeteilten Probleminstanzen durch. Zur Aufteilung der Berechnungsmenge wird eine Arbeitsanfrage eines Workers an den Master mit dem Intervall der als nächstes zu prüfenden Zahlen beantwortet, wobei jeweils die untere und obere Intervallgrenze ausreichen, wodurch 2 skalare Werte ein Arbeitspaket bilden. Mittels eines Parameters $k$ wird die Intervallgröße gesteuert, die mittels $\frac{n^2}{k}$ berechnet wird. \par
Initial verteilt der Master an jeden Worker ein Paket mit einer synchronen Sendeoperation. Daraufhin wird ein \emph{asynchroner Receive} auf jeden Worker gesetzt. In einem Request-Array werden die entsprechenden ``Anfragetickets'' abgelegt. Daraufhin beginnt eine Schleife, die terminiert, sobald alle Probleminstanzen abgearbeitet wurden. In dieser Schleife wird zu Beginn \emph{MPI\_Waitsome} verwendet, die auf einige abgeschlossene Empfangsoperationen wartet. Die zugehörigen PEs zu den Empfangsdaten haben ihr Arbeitspaket abgearbeitet und warten nun auf neue Arbeit. \emph{MPI\_Waitsome} liefert die Indizes dieser PEs, an die daraufhin neue Probleminstanzen zur Berechnung gesendet werden. Nach jedem Versandt wird eine asynchrone Receive-Operation vom Master gestartet, um später die Ergebnisse zu erhalten.\par
Sind alle Pakete abgearbeitet, sendet der Master an jeden Worker ein \emph{DIETAG}, welches das Ende der Arbeit indiziert. Jeder Worker weiß nun, dass es keine Arbeit mehr gibt und er terminieren kann.\par
Das Master-Worker-Schema kann generell für Probleme, die sich in Teilprobleme zerlegen lassen, als dynamisches Lastverfahren eingesetzt werden. Problematisch ist jedoch das Stichwort \emph{Engpass}, da der Master einen ``Single Point of Failure'' darstellt, denn sobald die Kommunikationsverbindung zum Master zu langsam wird, leidet die gesamte Lastverteilung darunter. 

% \section{Adaptive Granularitätskontrolle mit zufälligem Anfragen} % (fold)
% \label{sec:adaptiveGran}

% TODO