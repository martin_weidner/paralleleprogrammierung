void worker(Work work, int peId, int numProcs)
{ 
    ...
    for (int ptr = peId; ptr < resolution*resolution; ptr += numProcs) {
        if ( iterate(work.start, peId) == maxiter ) 
        {
            result[elem] = work.start;
            elem++;
        }
    }
    ...
}

