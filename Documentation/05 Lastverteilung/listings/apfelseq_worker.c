...
result = (int*)malloc(resolution * resolution * sizeof(int) + 1);
assert(result != 0);
  
elem = 1;
result[0] = work.start; 
printf("starting work on %d to %d\n", work.start, work.end);
for (; work.start <= work.end;  work.start++) 
{
    if (iterate(work.start) == maxiter) 
	{
        result[elem] = work.start;
        elem++;
    }
}
result[0] = work.end - result[0] + 1;
MPI_Send(result, elem, MPI_INT, 0, DISPLAY, MPI_COMM_WORLD);  
free(result);
...