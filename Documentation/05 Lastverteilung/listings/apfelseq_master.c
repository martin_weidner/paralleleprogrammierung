...
int *recvBuffer = (int*)malloc(resolution * resolution * sizeof(int));
assert(recvBuffer != 0);
if(withgraph) 
{    
    bitmapInit(resolution, resolution, display, "blub");
}
MPI_Recv(recvBuffer, resolution*resolution, MPI_INT, 
         MPI_ANY_SOURCE, DISPLAY, MPI_COMM_WORLD, &status);
MPI_Get_count(&status, MPI_INT, &length);

for (i = 1;  i < length;  i++) 
{
    x = recvBuffer[i] % resolution;
    y = recvBuffer[i] / resolution;
    bitmapSetPixel(x, resolution - y - 1);
}
if(withgraph)
{	
    bitmapDisplay();
}
free(recvBuffer);
...