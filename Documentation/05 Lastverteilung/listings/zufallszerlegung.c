int n = resolution;
int lower = (n*n / nProc) * iProc;
int upper = (n*n / nProc) * (iProc +1) - 1;
for (int r = 0; r<its; r++) {
        startTime = MPI_Wtime();

        work.start = 0;
        work.end = n*n - 1;
        worker( randomField, iProc );
        stopTime = MPI_Wtime() - startTime;
        MPI_Reduce( &stopTime, &timing[r], 1, MPI_DOUBLE, MPI_MAX, 0, MPI_COMM_WORLD );
    }
    
    if(!iProc){
        double pa = pruned_average(timing, its, 0.25);
        printf("%i\t %f\t %i\t %i #PEs\t #Time[us]\t #n*n\t  #Iterations\n",nProc, 1e6*stopTime, n*n, its); 
		fflush(stdout);
        free(timing);
    }

    free( randomField );
    MPI_Finalize();
  return 0;
}

