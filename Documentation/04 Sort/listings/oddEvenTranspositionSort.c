int oddEvenSort(int myItem)
{
	int i, otherItem;
	
	for(i = 0; i <= nProc; i++)
	{
		if((iProc & 1) == (i & 1))
		{
			if(iProc != 0)
			{ // exchange data myItem with left neighbor
				MPI_Ssend(&myItem, 1, MPI_INT, iProc - 1, 42, MPI_COMM_WORLD);
				MPI_Recv(&otherItem, 1, MPI_INT, iProc - 1, 42, MPI_COMM_WORLD, &dummy);
				if(myItem < otherItem)
				{
					myItem = otherItem;
				}
			}
		}
		else
		{
			if(iProc + 1 < nProc)
			{ // exchange data myItem with right neighbor
				MPI_Recv(&otherItem, 1, MPI_INT, iProc + 1, 42, MPI_COMM_WORLD, &dummy);
				MPI_Ssend(&myItem, 1, MPI_INT, iProc - 1, 42, MPI_COMM_WORLD);
				if(otherItem < myItem)
				{
					myItem = otherItem;
				}
			}
		}
	}
	return myItem;
}