int array_sum(int *array, int n){
    int res = 0;
    for (int i=0; i<n; i++) {
        res += array[i];
    }
    return res;
}

void exclusive_prefix(int *target, int *array, int numProcs){
    int sum = 0;
    for (int j=0; j<numProcs; j++) 
    {
        target[j] = sum; //start[j]-unsortedInput;
        sum += array[j];
    }
}