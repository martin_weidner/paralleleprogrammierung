randInitBound(randSamples, m, n);

for (i = 0; i < m; i++)
{ 
    randSamples[i] = unsortedInput[randSamples[i]];
}
MPI_Gather(randSamples, m, MPI_INT, allRandSamples, m, MPI_INT, ROOT, MPI_COMM_WORLD);

if(peId == ROOT)
{ 
    intSort(allRandSamples, globalSampleSize);
    for(i = 1; i < k-1; i++)
    { 
        pivot[i] =  allRandSamples[i*m]; 
    }
}
MPI_Bcast(pivot, k, MPI_INT, ROOT, MPI_COMM_WORLD);
partition(unsortedInput, n, pivot, start, k);
    
int n_elements = 0;
for (int j=0; j<numProcs; j++) 
{
    n_elements += start[j+1] - start[j];
    bucketlaenge[j] = start[j+1] - start[j];
}
MPI_Alltoall( bucketlaenge, 1, MPI_INT, empfangslaenge, 1, MPI_INT, MPI_COMM_WORLD );  
    
exclusive_prefix(send_displ, bucketlaenge, numProcs);
exclusive_prefix(recv_displ, empfangslaenge, numProcs);
i_get = array_sum(empfangslaenge, numProcs);

MPI_Alltoallv(unsortedInput, bucketlaenge, send_displ, MPI_INT, 
              finalSequence, empfangslaenge, recv_displ, MPI_INT, MPI_COMM_WORLD);
    
intSort(finalSequence, i_get);

assert(isGloballySorted(finalSequence, i_get));