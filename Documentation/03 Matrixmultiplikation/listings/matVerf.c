void verifyIdentMatrix(double *mat, int n)
{
	double variance = 0.001;
	for (int i=0;i<n; i++) 
	{
		for (int j=0; j<n; j++) 
		{
			if(fabs(mat[i*n+j]-(double)i/(j+1))-variance > 0)
			{
				return;
			}
		}
	}
	[...]
}