#ifndef INITMATRIX_H
#define INITMATRIX_H

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <random.h>

void matInitNull(double *mat, unsigned int n);
void matInitIdentity(double *mat, unsigned int n);
void matInitRandom(double *mat, unsigned int n, int seed);
void matInitMatrixA(double *mat, unsigned int n);
void matInitMatrixB(double *mat, unsigned int n);
void matInitEnumerate(double *mat, int n);
void matInitIdent(double *mat, int n);
void matInit12(double *mat, int n);

#endif /*INITMATRIX_H*/