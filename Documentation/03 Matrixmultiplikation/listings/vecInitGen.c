#ifndef INITMATRIX_H
#define INITMATRIX_H

#include <string.h>
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <random.h>

void vecInitEnumerate(double *vec, int n);
void vecInitIdent(double *vec, int n);
void vecInitZero(double *vec, int n);

#endif /*INITMATRIX_H*/
