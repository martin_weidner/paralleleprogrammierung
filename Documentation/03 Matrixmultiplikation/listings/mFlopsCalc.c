if(peId == 0)
{
		
	pa = pruned_average(timeValues, 10*numProcs, 0.25);
    double mFlops = (2*pow(n, 3) - pow(n,2))/(1e6*pa);
    printf("\n#PEs\t #Time[us]\t #MFlops \t #n\t #N \t #Iterations\n");
    printf("%i\t %f\t %f\t %i\t %i \t%i\n\n",numProcs, 1e6*pa, mFlops, n, blockDim, its); 
	flush(stdout);
}