if(peId == 0)
{ 
	for(i = 0; i < sqrtP; i++)
	{
		for(j = 0; j < sqrtP; j++)
		{
			tCoord[0] = i;
			tCoord[1] = j;
			MPI_Cart_rank(comm, tCoord, &tRank);
			displs[tRank] = offset = (n*blockDim*i)+(j*blockDim);
			MPI_Isend(&matA[offset], 1, MPI_subMatrix, tRank, COMM_A, comm, &requests[targetRank]);
			MPI_Isend(&matB[offset], 1, MPI_subMatrix, tRank, COMM_B, comm, &requests[numProcs+targetRank]);
		}
	}
}
MPI_Irecv(localA, blockDim*blockDim, MPI_DOUBLE, 0, COMM_A, comm, &req[0]);
MPI_Irecv(localB, blockDim*blockDim, MPI_DOUBLE, 0, COMM_B, comm, &req[1]);
MPI_Waitall(2, req, stat);