MPI_Cart_shift(comm, 1, -1*myCoord[0], &source, &dest);
MPI_Sendrecv_replace(localA, blockDim*blockDim, MPI_DOUBLE, dest, 0, source, 0, comm, &status);
MPI_Cart_shift(comm, 0, -1*myCoord[1], &source, &dest);
MPI_Sendrecv_replace(localB, blockDim*blockDim, MPI_DOUBLE, dest, 1, source, 1, comm, &status);