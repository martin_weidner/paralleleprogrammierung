void matInitMatrixA(double *mat, unsigned int n)
{
	for(int row=0; row<n; row++)
	{
		for(int col=0; col<n; col++)
		{
			mat[n*row+col]= (double)row/(col+1);
		}
	}
}

void matInitMatrixB(double *mat, unsigned int n)
{
	for(int row=0; row<n; row++)
	{
		for(int col=0; col<n; col++)
		{
			mat[n*row+col]= (double)(row+1)/(n*(col+1));
		}
	}
}