	linesPerPE = (int) (n/numProcs);
	[...]
	MPI_Scatter(mat, n*linesPerPE, MPI_DOUBLE, localMat, n*linesPerPE, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
	MPI_Bcast(x, n, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);
	cblas_dgemv(CblasRowMajor, CblasNoTrans, (linesPerPE), (n), 1.0, (localMat), (n), (x), 1, 0.0, (localY), 1) ;
	MPI_Gather(localY, linesPerPE, MPI_DOUBLE, y, linesPerPE, MPI_DOUBLE, ROOT, MPI_COMM_WORLD);