dim[0] = dim[1] = sqrtP;
reorder = 0;
period[0] = period[1] = 1;
MPI_Cart_create( MPI_COMM_WORLD, DIM, dim, period, reorder, &comm);
MPI_Type_vector(1, blockDim, 0, MPI_DOUBLE, &MPI_line);
MPI_Type_commit(&MPI_line);
MPI_Type_vector(blockDim, 1, (n/blockDim), MPI_line, &MPI_subMatrix);
MPI_Type_commit(&MPI_subMatrix);